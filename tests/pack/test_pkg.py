
from tucan.travel_in_package import find_package_files_and_folders
from tucan.package_analysis import run_struct
import json,os



def _same_item(out,ref,path=""):
    
    if type(out) is dict:
        _same_dict(out,ref,path=path)
    elif type(out) is list:
        _same_list(out,ref,path=path)
    else:
        assert out == ref
    return True

def _same_dict(out,ref,path=""):
    assert out.keys() == ref.keys()
    for key in out.keys():
        _same_item(out[key],ref[key],path=path+"."+ str(key))
    return True

def _same_list(out,ref,path=""):
    assert len(out) == len(ref)
    for i, _ in enumerate(out):
        _same_item(out[i], ref[i],path= path+"."+ str(i))
    return True
  



def test_pkg_ftn(datadir):
    paths_dir = find_package_files_and_folders(os.path.join(datadir,"fortran"))
    out = run_struct(paths_dir)
    with open(str(datadir) +"/package_analysis_ftn.json","r") as fin:
        ref_out = json.load(fin) 
    
    # same keys
    assert _same_item(out,ref_out)


def test_pkg_py(datadir):
    paths_dir = find_package_files_and_folders(os.path.join(datadir,"python"))
    #files = get_package_files(paths, datadir)
    #out = run_full_analysis(files)
    out = run_struct(paths_dir)
    with open(str(datadir) +"/package_analysis_py.json","r") as fin:
        ref_out = json.load(fin) 

    assert _same_item(out,ref_out)


def test_pkg_ccpp(datadir):
    paths_dir = find_package_files_and_folders(os.path.join(datadir,"c_cpp"))
    #files = get_package_files(paths, datadir)
    #out = run_full_analysis(files)
    out = run_struct(paths_dir)
    with open(str(datadir) +"/package_analysis_ccpp.json","r") as fin:
        ref_out = json.load(fin) 
   
    assert _same_item(out,ref_out)
