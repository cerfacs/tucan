# from typing import Tuple
from tucan.unformat_common import (
    clean_c_comments,
    strip_c_comment,
    new_stmts,
    Statements,
)
from tucan.unformat_c import (
    flag_declarations,
    merge_parenthesis_lines,
    split_blocks,
    merge_lines_by_end,
    line_to_flag,
)
from tucan.string_utils import tokenize
from tucan.struct_c import read_chevrons, read_template_name_and_type, build_c_ctrl_points
from tucan.imports_c import get_imports_ccpp


# Testing comments stripping ========================


def test_strip_c_comments():
    assert strip_c_comment("") == ""
    assert strip_c_comment(" ") == " "
    assert strip_c_comment("   ") == "   "

    assert strip_c_comment("Hello // comment ") == "Hello "
    assert strip_c_comment("Hello // comment ", fortran=True) == "Hello // comment "

    # mix one way
    assert strip_c_comment("Hello // horrible /* comment */") == "Hello "
    assert (
        strip_c_comment("Hello // horrible /* comment */", fortran=True)
        == "Hello // horrible "
    )
    assert strip_c_comment('Hello worse ab/* comment */cd') == 'Hello worse abcd'
    assert strip_c_comment('Hello // worse ab/* comment */cd') == 'Hello '
    
    assert strip_c_comment("Hello /* comment ") == "Hello "
    assert strip_c_comment("Hello /* comment */ there") == "Hello  there"
    assert (
        strip_c_comment("Hello /* comment */ there /* comment */ ") == "Hello  there  "
    )
    # mix other way
    assert strip_c_comment("Hello /* comment with // */ there") == "Hello  there"
    

def test_clean_c_comments():
    in_ = []
    ref_ = []
    assert clean_c_comments(in_) == ref_

    in_ = [""]
    ref_ = [""]
    assert clean_c_comments(in_) == ref_

    in_ = """
Hello /* comment */
there
/* is a pigeon
sleeping here */
no?
""".splitlines()
    ref_ = """
Hello 
there


no?
""".splitlines()
    assert clean_c_comments(in_) == ref_

    in_ = """
//***********************************************************************************
//**  COPYRIGHT (C) 2006-2010, V. Moureau and CORIA - CNRS,  ALL RIGHTS RESERVED.  **
#include "wx/wxprec.h"

""".splitlines()

    ref_ = """


#include "wx/wxprec.h"

""".splitlines()
    assert clean_c_comments(in_) == ref_

    in_ = ["Hello /* comment */ there /* is a pigeon sleeping here */ no?"]
    ref_ = ["Hello  there  no?"]
    assert clean_c_comments(in_) == ref_

    in_ = """
Hello there
!$ACC PARALLEL LOOP PRIVATE(switch1, switch2, zeta, ep4, ep2_p, ep2_y, diffw1, diffw2) DEFAULT(PRESENT) PRESENT(grid) IF ( using_acc )
!$ACC LOOP COLLAPSE(2)
is a pigeon sleeping here no?
""".splitlines()
    assert clean_c_comments(in_) == in_


    in_ = """
Hello there
Ìf distance
""".splitlines()
    out_ = """
Hello there
If distance
""".splitlines()
    assert clean_c_comments(in_) == out_

# unformatting C


def test_merge_parenthesis_lines():
    source = """
    dummy (
    dum,dum
    
    )

""".splitlines()
    out = merge_parenthesis_lines(new_stmts(source))
    print(new_stmts(source))
    assert out.lines == [[1, 1], [2, 5], [6, 6]]
    assert out.stmt == ["", "    dummy ( dum,dum  )", ""]


def test_merge_lines_by_end():
    source = """
    dummy
    dumpy:
    dam dam
    dam dam;

    foo (bar)
       { lorem ipsum }

    done
""".splitlines()
    out = merge_lines_by_end(new_stmts(source))

    target = """    dummy dumpy:
    dam dam dam dam;
    foo (bar) { lorem ipsum }
    done
""".splitlines()
    print("out    :", out.stmt)
    print("target :", target)

    assert out.stmt == target
    assert out.lines == [[2, 3], [4, 5], [7, 8], [10, 10]]


def test_split_blocks():
    source = """
    dummy { dum dum dum }
""".splitlines()

    out = split_blocks(new_stmts(source))
    print(new_stmts(source))
    print(out)
    assert out.lines == [[2, 2], [2, 2], [2, 2]]
    assert out.stmt == ["dummy {", "  dum dum dum ", "}"]

    # test to remove a semicolon on same line
    source = """
    dummy { dum dum dum };
""".splitlines()

    out = split_blocks(new_stmts(source))
    print(new_stmts(source))
    print(out)
    assert out.lines == [[2, 2], [2, 2], [2, 2]]
    assert out.stmt == ["dummy {", "  dum dum dum ", "}"]

    # test to remove a semicolon on next line
    source = """
    dummy { dum dum dum }
    ;
    birdy = a ;
    
""".splitlines()

    out = split_blocks(new_stmts(source))
    print(new_stmts(source))
    print(out)
    assert out.lines == [[2, 2], [2, 2], [2, 2], [4, 4]]
    assert out.stmt == ["dummy {", "  dum dum dum ", "}", "birdy = a ;"]


def test_split_blocks_nested():
    source = """
    dummy { birdy { num num }  }
""".splitlines()

    out = split_blocks(new_stmts(source))
    print(new_stmts(source))
    print(out)
    assert out.lines == [[2, 2], [2, 2], [2, 2], [2, 2], [2, 2]]
    assert out.stmt == ["dummy {", "  birdy {", "    num num ", "  }", "}"]


def test_split_blocks_mline():
    source = Statements()
    source.lines = [[1, 3], [1, 3], [1, 3]]
    source.stmt = ["dummy {", "  dum dum dum ", "}"]

    out = split_blocks(source)
    print(source)
    print(out)
    assert out.lines == source.lines
    assert out.stmt == source.stmt

    source = Statements()
    source.lines = [[1, 3], [1, 3]]
    source.stmt = ["dummy", "{dum dum dum}"]
    out = split_blocks(source)
    print(source)
    print(out)
    assert out.lines == [[1, 3], [1, 3], [1, 3]]
    assert out.stmt == ["dummy {", "  dum dum dum", "}"]


def test_line_to_flag():
    assert line_to_flag("bliblablo", 1) == False
    assert line_to_flag("template bliblablo", 1) == True
    assert line_to_flag("dummy {", 1) == False
    assert line_to_flag("dummy dummy1 {", 1) == True
    assert line_to_flag("int mpiwrapper::nodecommsize() {", 1) == True


# def test_flag_declarations():

#     source="""
#     dummy (){

#     }
# """.splitlines()

#     ref="""
#     dummy (){ ###===============================

#     }
# """.splitlines()
#     out = flag_declarations(source)
#     for line, refline in zip(out, ref):
#         print(f"|{line}|<>|{refline}|")
#         assert line == refline


#     source="""
#     if (){
#     }
#     switch (){
#     }
#     case (){
#     }
#     else (){
#     }
# """.splitlines()


#     out = flag_declarations(source)
#     for line, refline in zip(out, source):
#         print(f"|{line}|<>|{refline}|")
#         assert line == refline


def test_read_chevrons():
    list_ = ["<", ">"]
    out = read_chevrons(list_)
    assert out == ([], 1)

    list_ = ["<", "aaa", ">"]
    out = read_chevrons(list_)
    assert out == (["aaa"], 2)

    list_ = ["<", "aaa", "bbb", ">"]
    out = read_chevrons(list_)
    assert out == (["aaa", "bbb"], 3)

    list_ = ["<", "aaa", "=", "bbb", ">"]
    out = read_chevrons(list_)
    assert out == (["aaa"], 4)

    list_ = ["<", "aaa", "...", ">"]
    out = read_chevrons(list_)
    assert out == (["aaa"], 3)

    list_ = ["<", "aaa", ",", "bbb", ">"]
    out = read_chevrons(list_)
    assert out == (["aaa", "bbb"], 4)


def test_read_template_name_and_type():
    source = "template <> inline MPI_Datatype GetMpiType<unsigned long>() { return MPI_LONG; }"
    name_ref = "GetMpiType.<unsigned long>"
    type_ref = "MPI_Datatype"
    comment_ref = "<>"
    name, type_, comment = read_template_name_and_type(tokenize(source))
    assert type_ref == type_
    assert name_ref == name
    assert comment_ref == comment

    source = "template <typename T, std::size_t T_Size> void __impl_AddGatherMasterTuple(GatherMasterTupleMap"
    name_ref = "__impl_AddGatherMasterTuple"
    type_ref = "void"
    comment_ref = "<T,std::size_t,T_Size>"
    name, type_, comment = read_template_name_and_type(tokenize(source))
    assert type_ref == type_
    assert name_ref == name
    assert comment_ref == comment

    source = "template <typename T_MeshReader> Mesh(T_MeshReader &reader, CommunicatorConcept &comm)"
    name_ref = "Mesh"
    type_ref = "constructor"
    comment_ref = "<T_MeshReader>"
    name, type_, comment = read_template_name_and_type(tokenize(source))
    assert type_ref == type_
    assert name_ref == name
    assert comment_ref == comment

    source = "template <> inline hid_t &GetDatatype<int>() { return H5T_NATIVE_INT; }"
    name_ref = "&GetDatatype.<int>"
    type_ref = "hid_t"
    comment_ref = "<>"
    name, type_, comment = read_template_name_and_type(tokenize(source))
    assert type_ref == type_
    assert name_ref == name
    assert comment_ref == comment


def test_imports():
    imps = get_imports_ccpp([])
    assert imps == {}

    imps = get_imports_ccpp(["#include 'pack1.hpp'"])
    assert imps == {
        "_pack1.hpp": "pack1.hpp@*"
    }  # _pack because there is no explicit link to anything , like import *

    imps = get_imports_ccpp(["#include 'Pack1.hpp'"])
    assert imps == {
        "_Pack1.hpp": "Pack1.hpp@*"
    }  # _pack because there is no explicit link to anything , like import *


    imps = get_imports_ccpp(["#include 'folder/pack1.hpp'"])
    assert imps == {
        "_folder/pack1.hpp": "folder/pack1.hpp@*"
    }  # _pack because there is no explicit link to anything , like import *

    imps = get_imports_ccpp(["#  include  'pack1.hpp'"])
    assert imps == {
        "_pack1.hpp": "pack1.hpp@*"
    }  # _pack because there is no explicit link to anything , like import *

    imps = get_imports_ccpp(["#include <mpi.h>"])
    assert imps == {
        "_mpi.h": "mpi.h@*"
    }  
    imps = get_imports_ccpp(["#include <folder/mpi.h>"])
    assert imps == {
        "_folder/mpi.h": "folder/mpi.h@*"
    }  # _pack because there is no explicit link to anything , like import *


def test_build_c_ctrl_points():

     # void input
    ctrplt = build_c_ctrl_points([[]])
    assert ctrplt == []


    ref = """#include <stdio.h>
int main() {
    // Your main code goes here
    int x = 1;  // Example values for demonstration
    int y = 1;
    char a = 'a';
    if (x > 0) {
        if (y > 0) {
            y = 1.0;
        }
    }
    if (x > 0) {
        if (y > 0) {
            y = 1.0;
            switch (a) {
                case 'a':
                    // New if statement inside case 'a'
                    if (x == 1) {
                        printf("x is equal to 1 inside case 'a'\n");
                    }
                    break;
                case 'b':
                    // Code for case 'b'
                    break;
                default:
                    // Default case
                    break;
            }
        }
    }
    return 0;
}
"""

    tokencode = [tokenize(line) for line in ref.splitlines()]

    ctrplt = build_c_ctrl_points(tokencode)
    #print("result",ctrplt)
    assert ctrplt == ['if', ['if', []], 'if', ['if', ['case', [], 'if', [], 'case', [], 'default', []]]]
#            