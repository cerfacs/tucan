
from pytest import approx as approx_equal
from tucan.complexities import compute_control_pts,compute_possible_paths,halstead_numbers,halstead_properties
from tucan.struct_common import normalize_score, complexity_score
import numpy as np

def test_cyclo():
    # no stack gives one path
    # stack = []
    # assert compute_cyclomatic_complexity(stack) == 1
    # assert compute_possible_paths(stack) == 1
    active_ctrl_pts = ["if","select_case"]
    passive_ctrl_pts = ["else","elif","case"]
    stack = ["if",[]]
    assert compute_control_pts(stack, active_ctl_pts=active_ctrl_pts, passive_ctl_pts=passive_ctrl_pts) == 1
    assert compute_possible_paths(stack, active_ctl_pts=active_ctrl_pts, passive_ctl_pts=passive_ctrl_pts) == 2

    stack = ["if",[], "else",[]]
    assert compute_control_pts(stack, active_ctl_pts=active_ctrl_pts, passive_ctl_pts=passive_ctrl_pts) == 1
    assert compute_possible_paths(stack, active_ctl_pts=active_ctrl_pts, passive_ctl_pts=passive_ctrl_pts) == 3 #TODO should be 2 

    stack = ["if",[],"if",[]]
    assert compute_control_pts(stack, active_ctl_pts=active_ctrl_pts, passive_ctl_pts=passive_ctrl_pts) == 2
    assert compute_possible_paths(stack, active_ctl_pts=active_ctrl_pts, passive_ctl_pts=passive_ctrl_pts) == 4

    stack = ["if",[], "elif",[]]
    assert compute_control_pts(stack, active_ctl_pts=active_ctrl_pts, passive_ctl_pts=passive_ctrl_pts) == 1
    assert compute_possible_paths(stack, active_ctl_pts=active_ctrl_pts, passive_ctl_pts=passive_ctrl_pts) == 3 

    stack = ["select_case",[], "case",[], "case",[]]
    assert compute_control_pts(stack, active_ctl_pts=active_ctrl_pts, passive_ctl_pts=passive_ctrl_pts) == 1
    assert compute_possible_paths(stack, active_ctl_pts=active_ctrl_pts, passive_ctl_pts=passive_ctrl_pts) == 4 #TODO should be 3

    stack = ["if",[], "if",[], "if",[], "if",[]]
    assert compute_control_pts(stack, active_ctl_pts=active_ctrl_pts, passive_ctl_pts=passive_ctrl_pts) == 4
    assert compute_possible_paths(stack, active_ctl_pts=active_ctrl_pts, passive_ctl_pts=passive_ctrl_pts) == 16

    stack = ["if", ["if", []]]
    assert compute_control_pts(stack, active_ctl_pts=active_ctrl_pts, passive_ctl_pts=passive_ctrl_pts) == 2
    assert compute_possible_paths(stack, active_ctl_pts=active_ctrl_pts, passive_ctl_pts=passive_ctrl_pts) == 3

    stack = ["if", ["if", ["if", []]]]
    assert compute_control_pts(stack, active_ctl_pts=active_ctrl_pts, passive_ctl_pts=passive_ctrl_pts) == 3
    assert compute_possible_paths(stack, active_ctl_pts=active_ctrl_pts, passive_ctl_pts=passive_ctrl_pts) == 4

    # nested ifs
    stack = ["if", ["if", ["if", ["if", []]]]]
    assert compute_control_pts(stack, active_ctl_pts=active_ctrl_pts, passive_ctl_pts=passive_ctrl_pts) == 4
    assert compute_possible_paths(stack, active_ctl_pts=active_ctrl_pts, passive_ctl_pts=passive_ctrl_pts) == 5

    stack = ["if", ["if", [], "else", []], "else", [], "if", [], "else", []]
    assert compute_control_pts(stack, active_ctl_pts=active_ctrl_pts, passive_ctl_pts=passive_ctrl_pts) == 3
    assert compute_possible_paths(stack, active_ctl_pts=active_ctrl_pts, passive_ctl_pts=passive_ctrl_pts) == 11 #TODO  expected 6
    
    
def test_halstead_numbers():
    from tucan.string_utils import tokenize
    # c example from https://en.wikipedia.org/wiki/Halstead_complexity_measures
    code= """
main()
{
  int a, b, c, avg;
  scanf("%d %d %d", &a, &b, &c);
  avg = (a+b+c)/3;
  printf("avg = %d", avg);
}
"""
    tkn_code=[tokenize(line) for line in code.splitlines()]
    optr_tot,oprd_tot,optr_set,oprd_set = halstead_numbers(tkn_code, ["main","int","scanf", "printf"])
    assert optr_tot == 27
    assert oprd_tot == 15
    assert optr_set == 12
    assert oprd_set == 7
    
def test_halstead_properties():
    volume, difficulty, effort = halstead_properties(27,15,12,7)
    assert approx_equal(volume,0.01)==178.4
    assert approx_equal(difficulty,0.01)==12.85
    assert approx_equal(effort,0.01)==2292.44


## ------ TEST NORMALIZE_SCORE ------ ##


def test_normalize_score():
    in_ = np.array([1, 10, 20, 30, 40, 50])
    val_for_0 = 10
    factor_for_10 = 100
    assert np.allclose(
        normalize_score(in_, val_for_0, factor_for_10=factor_for_10),
        np.array([0, 0, 1.5, 2.38, 3.01, 3.49]),
        atol=0.01,
    )


def test_normalize_score_below_threshold():
    in_ = np.array([0, 5, 8, 9])
    val_for_0 = 10
    assert (normalize_score(in_, val_for_0) == np.array([0, 0, 0, 0])).all()


def test_normalize_score_same_value_as_val_for0():
    in_ = np.array([10])
    val_for_0 = 10
    factor_for_10 = 60
    assert normalize_score(in_, val_for_0, factor_for_10) == np.array([0])


## ------ TEST COMPLEXITY_SCORE ------ ##


def test_complexity_score():
    # Sample input data
    joined_complexity = {
            "CCN": [12, 15, 20],
            "IDT_int": [60, 80, 100],
            "NLOC": [100, 150, 200],
            "HDF": [6, 8, 10],
        }

    # Expected output (simplified for the sake of demonstration)
    expected_output = {
            "score": (
                normalize_score(np.array([12, 15, 20]), 10.0)
                + normalize_score(np.array([60, 80, 100]), 1.0)
                + normalize_score(np.array([100, 150, 200]), 50.0)
                + normalize_score(np.array([6, 8, 10]), 50.0)
            )
            / 4.0,
            "cyclomatic": normalize_score(np.array([12, 15, 20]), 10.0),
            "indentation": normalize_score(np.array([60, 80, 100]), 1.0),
            "halstead_diff": normalize_score(np.array([6, 8, 10]), 50.0),
            "size": normalize_score(np.array([100, 150, 200]), 50.0),
    }

    result = complexity_score(joined_complexity)

    # Compare the result with the expected output
    for key in expected_output:
        assert np.allclose(result[key], expected_output[key], atol=0.01)