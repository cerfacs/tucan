from tucan.string_utils import (
    find_words_before_left_parenthesis_noregexp as fwblp,
    iterate_with_neighbors,
    tokenize,
    get_indent,
    string_decode,
    string_encode,
    metachar_encode,
    metachar_decode,
    get_common_root_index,
    get_common_root_index_list,
    front_match,
    best_front_match,
    inner_match,
    best_inner_match,
    generate_dict_paths,
)


def test_get_indent():
    assert get_indent("") == ""
    assert get_indent("   ") == "   "
    assert get_indent("   test") == "   "
    assert get_indent("   test   ") == "   "
    assert get_indent("\ttest") == "    "


def test_iterate_with_neighbors():
    assert list(iterate_with_neighbors("abc")) == [
        (None, "a"),
        ("a", "b"),
        ("b", "c"),
        ("c", None),
    ]


def test_str_encode_decode():
    assert string_encode("abc, def ' titi'") == "abc, def ú titiû"
    assert string_encode('abc, def " titi"') == "abc, def ú titiû"
    assert string_decode(string_encode("abc, def ' titi'")) == 'abc, def " titi"'
    assert string_decode(string_encode('abc, def " titi"')) == 'abc, def " titi"'

def test_metachar_encode_decode():
    assert metachar_encode("abc::def") == "abcÍdef"
    assert metachar_decode("abcÍdef") == "abc::def"
    

def test_tokenize():
    assert tokenize("") == []
    assert tokenize("dummy") == ["dummy"]
    assert tokenize("dum dum dum") == ["dum", "dum", "dum"]
    assert tokenize("dum::dum dum") == ["dum::dum", "dum"]
    assert tokenize("dum%dum dum") == ["dum%dum", "dum"]
    assert tokenize("dum_dum dum") == ["dum_dum", "dum"]
    assert tokenize("dum.dum dum") == ["dum.dum", "dum"]

    assert tokenize("dum,dum,dum") == ["dum", ",", "dum", ",", "dum"]
    assert tokenize("dum , dum , dum") == ["dum", ",", "dum", ",", "dum"]
    assert tokenize("dum.dum.dum") == ["dum.dum.dum"]
    assert tokenize("dum_dum_dum") == ["dum_dum_dum"]
    assert tokenize("dum123 123dum du123m") == ["dum123", "123dum", "du123m"]
    assert tokenize("12 + 3.14") == ["12", "+", "3.14"]
    assert tokenize("dummy({[3.14e16]})") == [
        "dummy",
        "(",
        "{",
        "[",
        "3.14e16",
        "]",
        "}",
        ")",
    ]
    assert tokenize("dummy, test") == ["dummy", ",", "test"]
    assert tokenize("dummy , test") == ["dummy", ",", "test"]
    assert tokenize("dummy ,test") == ["dummy", ",", "test"]
    assert tokenize("XI**(-1.0/6.0)") == ["XI", "**", "(", "-", "1.0", "/", "6.0", ")"]

    assert tokenize("LOREM IPSUM ' tireli pin pon ' ") == [
        "LOREM",
        "IPSUM",
        '" tireli pin pon "',
    ]
    assert tokenize('LOREM IPSUM " tireli pin pon " ') == [
        "LOREM",
        "IPSUM",
        '" tireli pin pon "',
    ]
    assert tokenize('LOREM IPSUM " unfinished ') == ["LOREM", "IPSUM", '" unfinished ']
    assert tokenize('LOREM IPSUM " tireli \' pin pon " ') == [
        "LOREM",
        "IPSUM",
        '" tireli \' pin pon "',
    ]


def test_find_words_before_left_parenthesis():
    assert fwblp(tokenize("")) == []
    assert fwblp(tokenize("test()")) == ["test"]
    assert fwblp(tokenize("test ()")) == ["test"]
    assert fwblp(tokenize("+test()")) == ["test"]
    assert fwblp(tokenize("!test()")) == ["test"]
    assert fwblp(tokenize("test(test())")) == ["test"]
    assert fwblp(tokenize("test(dummy())")) == ["dummy", "test"]
    assert fwblp(tokenize("part time(")) == ["time"]
    assert fwblp(tokenize("part+time(")) == ["time"]


def test_common_root_index():
    list_ = [
        "azerty123",
        "azerty",
        "azerty456",
        # 012345678
    ]
    assert get_common_root_index(list_) == 5

    # no hits
    list_ = [
        "afoo/bar",
        "bfoo/bar",
        "cfoo/bar",
        # 012345678
    ]
    assert get_common_root_index(list_) == -1


    list_ = [
        "",
        "azerty123",
        "azerty",
        "azerty456",
        # 012345678
    ]
    assert get_common_root_index(list_) == 0

    # WORKS ALSO WITH LISTS !!!
    list_ = [
        ["foo", "bar", "yacco"],
        ["foo", "bar", "wacco"],
        ["foo", "bar", "dot"],
        # 012345678
    ]
    assert get_common_root_index_list(list_) == 1

     # no hits
    list_ = [
        ["afoo", "bar", "yacco"],
        ["bfoo", "bar", "wacco"],
        ["cfoo", "bar", "dot"],
        # 012345678
    ]
    assert get_common_root_index_list(list_) == -1

    list_ = [
        [],
        ["foo", "bar", "yacco"],
        ["foo", "bar", "wacco"],
        ["foo", "bar", "dot"],
        # 012345678
    ]
    assert get_common_root_index_list(list_) == 0


def test_front_match():
    assert front_match(["a", "b", "c"], ["z", "b", "c"]) == 0
    assert front_match(["a", "b", "c"], ["a", "b", "z"]) == 2
    assert front_match("abc321", "abc567") == 3


def test_best_front_match():
    list_ = ["abc1", "abcde", "abcfg"]
    assert best_front_match("abcdefgh", list_) == "abcde"
    assert best_front_match("zozo", list_) == None
    assert best_front_match("abc", list_) == "abc1"

    list_ = [["a", "b", "c", "1"], ["a", "b", "c", "d", "e"], ["a", "b", "c", "f", "g"]]
    assert best_front_match(["a", "b", "c", "d", "e", "f", "g", "h"], list_) == [
        "a",
        "b",
        "c",
        "d",
        "e",
    ]


def test_inner_match():
    assert inner_match(["a", "b", "c"], ["d", "e", "f"]) == 0
    assert inner_match(["a", "b", "c"], ["a", "b", "c", "d", "e", "f"]) == 3
    assert inner_match(["d", "e", "f"], ["a", "b", "c", "d", "e", "f"]) == 3
    assert inner_match(["c", "d"], ["a", "b", "c", "d", "e", "f"]) == 2
    assert inner_match(["a", "b", "c", "d", "e", "f"], ["c", "d"]) == 2
    assert inner_match(["a", "b", "c"], ["b", "c", "d"]) == 2

    assert inner_match("nabo", "boooo") == 2


def test_best_inner_match():
    list_ = ["abcdef", "boooo", "cbcooa"]
    assert best_inner_match("iiiiiiiabiiiiiiii", list_) == [0]  # "abcdef"
    assert best_inner_match("cde", list_) == [0]  # "abcdef"
    assert best_inner_match("zzzzzz", list_) == []
    assert best_inner_match("nzbo", list_) == [1]  # "boooo"
    assert best_inner_match("oo", list_) == [1, 2]  # "boooo", "cbcooa


def test_dict_paths():
    # Example nested dictionary
    nested_dict = {"a": {"b": {"c": 1, "d": 2}, "e": {"f": 3}}, "g": {"h": 4}}
    expected_paths = [
        ["a", "b", "c"],
        ["a", "b", "d"],
        ["a", "e", "f"],
        ["g", "h"],
    ]
    assert generate_dict_paths(nested_dict) == expected_paths

    tree_dict = {
        "src": {
            "includes": {"one.hpp": {"one": {}}, "two.hpp": {"one": {}}},
            "core": {"main.cpp": {}, "add.cpp": {}},
        }
    }
    expected_paths = [
        ["src", "includes", "one.hpp", "one"],
        ["src", "includes", "two.hpp", "one"],
        ["src", "core", "main.cpp"],
        ["src", "core", "add.cpp"],
    ]
    assert generate_dict_paths(tree_dict) == expected_paths
