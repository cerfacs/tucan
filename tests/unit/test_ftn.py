from tucan.imports_ftn import get_imports_ftn
from tucan.string_utils import tokenize
from tucan.unformat_ftn import (
    align_start_continuations,
    ftn_clean_type_keywords,
    split_multi_statement_lines,
    split_multi_declaration_lines,
    ftn_align_labelled_continuations,
    ftn_clean_labelled_loops_oldftn,
    ftn_clean_labelled_loops_newftn,
    ftn_make_oneliners_conditionals_multilines,
    split_after_1st_parenthesis_group,
    align_multiline_blocks_ftn,
    ftn_remove_decorators
)
from tucan.unformat_common import new_stmts, align_multiline_blocks

from tucan.struct_ftn import build_ftn_ctrl_points


def test_ftn_align_labelled_continuations():
    """Labelled continuations"""

    # default feature
    source = new_stmts(
        """
   15 FORMAT(
     1 /' TRANFT:  Transport property fitting,',
     2 /'           CHEMKIN-II Version ',A,', August 1994',
     3 /'           DOUBLE PRECISION')
""".lower().splitlines()
    )
    out_ref = """
   15 FORMAT(/' TRANFT:  Transport property fitting,',/'           CHEMKIN-II Version ',A,', August 1994',/'           DOUBLE PRECISION')
""".lower().splitlines()
    out_test = ftn_align_labelled_continuations(source)
    assert out_test.to_code() == out_ref

    # No actions feature
    source = new_stmts(
        """
   15 FORMAT(
     /' TRANFT:  Transport property fitting,',
     /'           CHEMKIN-II Version ',A,', August 1994',
     /'           DOUBLE PRECISION')
""".lower().splitlines()
    )

    out_test = ftn_align_labelled_continuations(source)
    assert out_test == source


def test_ftn_clean_labelled_loops_oldftn():
    # source taken from alya, nb of spaces matter
    source = new_stmts(
        """
               DO 10, I = 1, N
                  Y( I ) = ZERO
   10          CONTINUE
""".lower().splitlines()
    )

    out_ref = new_stmts(
        """
               DO 10, I = 1, N
                  Y( I ) = ZERO
               END DO ! 10
""".lower().splitlines()
    )
    out_test = ftn_clean_labelled_loops_oldftn(source)
    assert out_test == out_ref

    # source taken from alya, nb of spaces matter
    source = new_stmts(
        """
                  DO 10,I = 1, J - 1
                     A( I, J ) = A( I, J ) + X( I )*TEMP
   10             CONTINUE
""".lower().splitlines()
    )

    out_ref = new_stmts(
        """
                  DO 10,I = 1, J - 1
                     A( I, J ) = A( I, J ) + X( I )*TEMP
                  END DO ! 10
""".lower().splitlines()
    )
    out_test = ftn_clean_labelled_loops_oldftn(source)
    #print("\n".join(out_test.to_code()))
    #print("\n".join(out_ref.to_code()))

    assert out_test == out_ref

    #Other alya contender with over labelled loops

    source =new_stmts("""
  100 do 120 jj = 1, l
         go to 20
  120 continue""".lower().splitlines())



    out_ref =new_stmts("""
  100 do 120 jj = 1, l
         go to 20
  end do ! 120""".lower().splitlines())
    out_test = ftn_clean_labelled_loops_oldftn(source)
    print("\n".join(out_test.to_code()))
    print("\n".join(out_ref.to_code()))
    assert out_test == out_ref
    # important to test this with a label nested inside
    source = new_stmts(
        """
      DO 1000 IDUMP=1,NDUMPS
   51   IF (NPS.NE.NPSCAL) THEN
          IF (NID.EQ.0) THEN 
          END IF
        END IF
      1000 CONTINUE
""".lower().splitlines()
    )

    out_ref = new_stmts(
        """
      DO 1000 IDUMP=1,NDUMPS
   51   IF (NPS.NE.NPSCAL) THEN
          IF (NID.EQ.0) THEN 
          END IF
        END IF
      END DO ! 1000
""".lower().splitlines()
    )
    out_test = ftn_clean_labelled_loops_oldftn(source)
    assert out_test == out_ref

    source = new_stmts(
        """
      DO 1000 IDUMP=1,NDUMPS
        DO 1000 JDUMP=1,NDUMPS
          DO 1000 KDUMP=1,NDUMPS
      1000 CONTINUE
""".lower().splitlines()
    )

    out_ref = new_stmts(
        """
      DO 1000 IDUMP=1,NDUMPS
        DO 1000 JDUMP=1,NDUMPS
          DO 1000 KDUMP=1,NDUMPS
          END DO ! 1000
        END DO ! 1000
      END DO ! 1000
""".lower().splitlines()
    )
    out_test = ftn_clean_labelled_loops_oldftn(source)
    assert out_test.to_code() == out_ref.to_code()


def test_ftn_clean_labelled_loops_newftn():

    source = """
    node_loop01: do inode = 1,pnode
          ipoin = lnods(inode, ielem) 
    enddo node_loop01
""".splitlines()
    
    ref = """
    do inode = 1,pnode ! NESTING_LABEL# node_loop01
          ipoin = lnods(inode, ielem) 
    enddo node_loop01
""".splitlines()
    
    out_test = ftn_clean_labelled_loops_newftn(source)
    print("\n".join(out_test))
    print("\n".join(ref))
    
    assert out_test == ref

def test_clean_type_keywords():
    """ensure the correct cleaning of types"""
    assert ftn_clean_type_keywords(["type(bcdhjzks)"]) == ["__type_from__ ( bcdhjzks )"]
    assert ftn_clean_type_keywords(["type is(adv_no_dealias_t)"]) == [
        "__type_is__ ( adv_no_dealias_t )"
    ]
    assert ftn_clean_type_keywords(["type, extend"]) == ["type , extend"]
    assert ftn_clean_type_keywords(["type_error = .true."]) == ["type_error = .true."]
    assert ftn_clean_type_keywords(["type isoparametric"]) == ["type isoparametric"]
    assert ftn_clean_type_keywords(["end type isoparametric"]) == [
        "end type isoparametric"
    ]


def test_split_multi_statement_lines():
    source = """
    real :: a ; real :: b
"""
    st_sc = new_stmts(source.splitlines())
    out = split_multi_statement_lines(st_sc).to_code()
    ref = """
    real :: a
    real :: b
""".splitlines()
    assert out == ref


def test_split_multi_declaration_lines():
    source = """
    procedure :: a, b
"""
    st_sc = new_stmts(source.splitlines())
    out = split_multi_declaration_lines(st_sc).to_code()
    ref = """
    procedure :: a
    procedure :: b
""".splitlines()
    assert out == ref

    source = """
    procedure a, b
"""
    st_sc = new_stmts(source.splitlines())
    out = split_multi_declaration_lines(st_sc).to_code()
    ref = """
    procedure a
    procedure b
""".splitlines()
    assert out == ref

    source = """
    procedure clutter,clutter :: a, b
"""
    st_sc = new_stmts(source.splitlines())
    out = split_multi_declaration_lines(st_sc).to_code()
    ref = """
    procedure clutter , clutter :: a
    procedure clutter , clutter :: b
""".splitlines()
    assert out == ref


def test_multiline_parenthesis():
    # normal multiline parenthesis
    source = """
    hello (/
    whatever ()
    /)
"""
    st_sc = new_stmts(source.splitlines())
    out = align_multiline_blocks_ftn(st_sc)

    ref = """
    hello (/ whatever () /)
"""
    st_ref = new_stmts(ref.splitlines())
    st_ref.lines[1] = [2, 4]

    assert out.stmt == st_ref.stmt
    print(out.lines)
    print(st_ref.lines)

    assert out.lines == st_ref.lines

    # wicked multiline parenthesis
    # the last /) is in fact ), and deal with it, while dealing with the previous one...

    source = """
    hello (/
    whatever ()
    )
    lorem ipsum
"""
    st_sc = new_stmts(source.splitlines())

    ref = """
    hello (/ whatever () )
    lorem ipsum
"""
    st_ref2 = new_stmts(ref.splitlines())
    st_ref2.lines[1] = [2, 4]
    st_ref2.lines[2] = [5, 5]

    out = align_multiline_blocks_ftn(st_sc)
    assert out.stmt == st_ref2.stmt
    assert out.lines == st_ref2.lines


def test_imports():
    imps = get_imports_ftn([])
    assert imps == {}

    imps = get_imports_ftn(["use pack1"])
    assert imps == {
        "_pack1": "__ftn__@pack1"
    }  # _pack because there is no explicit link to anything , like import *

    imps = get_imports_ftn(["use pack1 only def1,def2,def3"])
    assert imps == {
        "def1": "__ftn__@pack1.def1",
        "def2": "__ftn__@pack1.def2",
        "def3": "__ftn__@pack1.def3",
    }  # _pack because there is no explicit link to anything , like import *

    imps = get_imports_ftn(["include 'pack1.f90'"])
    assert imps == {
        "_pack1.f90": "pack1.f90@*"
    }  # _pack because there is no explicit link to anything , like import *

    imps = get_imports_ftn(["#include 'pack1.f90'"])
    assert imps == {
        "_pack1.f90": "pack1.f90@*"
    }  # _pack because there is no explicit link to anything , like import *

    imps = get_imports_ftn(["#  include  'pack1.f90'"])
    assert imps == {
        "_pack1.f90": "pack1.f90@*"
    }  # _pack because there is no explicit link to anything , like import *

    imps = get_imports_ftn(["use, intrinsic :: iso_c_binding"])
    assert imps == {
        "iso_c_binding": "__ftn__@__intrinsic__.iso_c_binding"
    }  # _pack because there is no explicit link to anything , like import *

    imps = get_imports_ftn(["use atchem, only : nespg"])
    assert imps == {
        "nespg": "__ftn__@atchem.nespg"
    }  # _pack because there is no explicit link to anything , like import *


def test_split_after_1st_parenthesis_group():
    line = "This is a test string without parentheses."
    result = split_after_1st_parenthesis_group(line)
    assert result == (line, "")

    line = "start (middle) end"
    result = split_after_1st_parenthesis_group(line)
    assert result == ("start (middle)", "end")

    line = "outer (inner (nested)) tail"
    result = split_after_1st_parenthesis_group(line)
    assert result == ("outer (inner (nested))", "tail")

    line = "(first) middle (second)"
    result = split_after_1st_parenthesis_group(line)
    assert result == ("(first)", "middle (second)")

    line = "text with (unmatched open"
    result = split_after_1st_parenthesis_group(line)
    assert result == (line, "")

    line = "text with unmatched close)"
    result = split_after_1st_parenthesis_group(line)
    assert result == (line, "")

    line = ""
    result = split_after_1st_parenthesis_group(line)
    assert result == ("", "")


def test_ftn_make_oneliners_conditionals_multilines():
    # IF ONE LINERS================================
    source = """if (x > 0) y = 1.0"""
    st_sc = new_stmts(source.splitlines())

    ref = """if (x > 0) then
    y = 1.0
end if"""
    st_ref = new_stmts(ref.splitlines())
    st_ref.lines = [[1, 1], [1, 1], [1, 1]]

    st_test = ftn_make_oneliners_conditionals_multilines(st_sc)
    # print(st_test)
    # print(st_ref)
    assert st_test == st_ref

    # ensure identity
    st_test = ftn_make_oneliners_conditionals_multilines(st_ref)
    # print(st_test)
    # print(st_ref)
    assert st_test == st_ref

    # WHERE ONE LINERS================================
    source = """where (array > 0.0) array = array * 2.0"""
    st_sc = new_stmts(source.splitlines())

    ref = """where (array > 0.0)
    array = array * 2.0
end where"""
    st_ref = new_stmts(ref.splitlines())
    st_ref.lines = [[1, 1], [1, 1], [1, 1]]

    st_test = ftn_make_oneliners_conditionals_multilines(st_sc)
    # print(st_test)
    # print(st_ref)
    assert st_test == st_ref

    # ensure identity
    st_test = ftn_make_oneliners_conditionals_multilines(st_ref)
    # print(st_test)
    # print(st_ref)
    assert st_test == st_ref


    # FORALL ONE LINERS================================
    source = """forall (idime=1:ndime) gpgre(idime,idime,igaus)=gpgre(idime,idime,igaus)-0.5_rp"""
    st_sc = new_stmts(source.splitlines())

    ref = """forall (idime=1:ndime)
    gpgre(idime,idime,igaus)=gpgre(idime,idime,igaus)-0.5_rp
end forall"""
    st_ref = new_stmts(ref.splitlines())
    st_ref.lines = [[1, 1], [1, 1], [1, 1]]

    st_test = ftn_make_oneliners_conditionals_multilines(st_sc)
    # print(st_test)
    # print(st_ref)
    assert st_test == st_ref

    # ensure identity
    st_test = ftn_make_oneliners_conditionals_multilines(st_ref)
    # print(st_test)
    # print(st_ref)
    assert st_test == st_ref


    # DO ONE LINERS================================
    # source = """do i = 1, 10 sum = sum + array(i)"""
    # st_sc = new_stmts(source.splitlines())

    # ref = """do i = 1, 10
    # sum = sum + array(i)
    # end do"""
    # st_ref = new_stmts(ref.splitlines())
    # st_ref.lines =[[1, 1], [1, 1], [1, 1]]

    # st_test =ftn_make_oneliners_conditionals_multilines(st_sc)
    # print(st_test)
    # print(st_ref)
    # assert st_test == st_ref

    # #ensure identity
    # st_test =ftn_make_oneliners_conditionals_multilines(st_ref)
    # # print(st_test)
    # # print(st_ref)
    # assert st_test == st_ref

    # NESTED ONE LINERS================================

    source = """    if (INOTMASTER) where( lagrtyp(:) % kfl_exist == -5) lagrtyp(:) % kfl_exist = -1"""
    st_sc = new_stmts(source.splitlines())

    ref = """    if (INOTMASTER) then
        where( lagrtyp(:) % kfl_exist == -5)
            lagrtyp(:) % kfl_exist = -1
        end where
    end if"""
    st_ref = new_stmts(ref.splitlines())
    st_ref.lines = [[1, 1], [1, 1], [1, 1], [1, 1], [1, 1]]

    st_test = ftn_make_oneliners_conditionals_multilines(st_sc)
    # print(st_test)
    # print(st_ref)
    assert st_test == st_ref


def test_align_start_continuations():
    source = """      if (poitr(1).ge.coorb(1,1,iboxf)-1.0e-06.and.
     .poitr(1).le.coorb(1,2,iboxf)+1.0e-06    ) then
        outsi=.false.   
      end if"""
    st_sc = new_stmts(source.splitlines())

    ref = """      if (poitr(1).ge.coorb(1,1,iboxf)-1.0e-06.and. poitr(1).le.coorb(1,2,iboxf)+1.0e-06    ) then
        outsi=.false.   
      end if"""
    st_ref = new_stmts(ref.splitlines())
    st_ref.lines = [[1, 2], [3, 3], [4, 4]]

    st_test = align_start_continuations(st_sc)
    # print(st_test)
    # print(st_ref)
    assert st_test == st_ref

    source = """      if (poitr(1).ge.coorb(1,1,iboxf)-1.0e-06.and.
     +poitr(1).le.coorb(1,2,iboxf)+1.0e-06    ) then
        outsi=.false.   
      end if"""
    st_sc = new_stmts(source.splitlines())

    ref = """      if (poitr(1).ge.coorb(1,1,iboxf)-1.0e-06.and. poitr(1).le.coorb(1,2,iboxf)+1.0e-06    ) then
        outsi=.false.   
      end if"""
    st_ref = new_stmts(ref.splitlines())
    st_ref.lines = [[1, 2], [3, 3], [4, 4]]

    st_test = align_start_continuations(st_sc)
    # print(st_test)
    # print(st_ref)
    assert st_test == st_ref


def test_build_ftn_ctrl_points():
    # void input
    ctrplt = build_ftn_ctrl_points([[]])
    assert ctrplt == []

    ref = """if (x > 0) then
    if (y > 0) then
        y = 1.0
    end if
end if
if (x > 0) then
    if (y > 0) then
        y = 1.0
        select case a
            case a
            case b
        end select
    end if
end if
"""

    tokencode = [tokenize(line) for line in ref.splitlines()]

    ctrplt = build_ftn_ctrl_points(tokencode)

    assert ctrplt == ["if", ["if", []], "if", ["if", ["case", [], "case", []]]]





def test_ftn_remove_decorators():
    # void input
    assert ( ftn_remove_decorators(
            "     function fun(a,b,c)", "function") 
        ==  "     function fun(a,b,c) ! FUNCTION# "
        )

    assert ( ftn_remove_decorators(
            "     endfunction", "function") 
        ==  "     end function"
        )

    assert ( ftn_remove_decorators(
            "     pure function fun(a,b,c)", "function") 
        ==  "     function fun(a,b,c) ! FUNCTION# pure "
        )

    assert ( ftn_remove_decorators(
            "     complex(rp) function ARMONICO(X,Y,Z,L,M)", "function") 
        ==  "     function ARMONICO(X,Y,Z,L,M) ! FUNCTION# complex(rp) "
        )
    #Bitchy ones!!!!
    #CPMD code  https://github.com/OpenCPMD/CPMD  in src/egointer_utils.mod.F90
    assert ( ftn_remove_decorators(
            "     public :: interface","interface")
        ==  "     public :: interface"
        )
    assert ( ftn_remove_decorators(
            "     subroutine interface(c0,c2,sc0,pme,gde,vpp,eigv)","interface")
        ==  "     subroutine interface(c0,c2,sc0,pme,gde,vpp,eigv)"
        )