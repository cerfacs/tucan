from tucan.unformat_common import (
    remove_strings,
)

from tucan.unformat_py import split_fstrings


def test_remove_strings():
    code = [" lorem ipsum sic hamet "]
    assert remove_strings(code, "'") == [" lorem ipsum sic hamet "]

    code = [" lorem 'ipsum' sic hamet "]
    assert remove_strings(code, "'") == [" lorem '___' sic hamet "]

    code = [' lorem "ipsum" sic hamet ']
    assert remove_strings(code, '"') == [' lorem "___" sic hamet ']

    code = [" lorem 'ipsum' sic 'hamet' "]
    assert remove_strings(code, "'") == [" lorem '___' sic '___' "]

    code = ['prefix = "".join([f"{word.upper()} " for word in prefix.split("_")])']
    assert remove_strings(code, '"') == [
        'prefix = "___".join([f"___" for word in prefix.split("___")])'
    ]


def test_split_fstrings():
    code = ['f" lorem {ipsum} sic " hamet ']
    assert split_fstrings(code, '"', "'") == ['f" lorem "{ipsum}" sic " hamet ']

    code = ['f"{word.upper()} " for ']
    assert split_fstrings(code, '"', "'") == ['f""{word.upper()}" " for ']
