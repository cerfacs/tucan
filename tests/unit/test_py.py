from tucan.string_utils import tokenize, get_indent
from tucan.unformat_py import enforce_4char_indents
from tucan.imports_py import get_imports_py
from tucan.struct_py import find_class_inheritance,build_py_ctrl_points

def test_indents():
    """ensure the correct indets"""

    ref_ = """
aaaa
    bbbb
        cccc
""".splitlines()

    assert enforce_4char_indents(ref_) == ref_

    in_ = """
aaaa
  bbbb
    cccc
""".splitlines()
    assert enforce_4char_indents(in_) == ref_

    in_irreg = """
aaaa
  bbbb
     cccc
""".splitlines()
    assert enforce_4char_indents(in_irreg) == ref_

    in_zero = """
aaaa
bbbb
cccc
""".splitlines()
    assert enforce_4char_indents(in_zero) == in_zero

def test_imports():
    imps = get_imports_py([]) 
    assert imps == {}

    imps = get_imports_py(["import pack1"])
    assert imps == {"pack1": "__py__@pack1"}

    imps = get_imports_py(["import pack1.mod1.smod2"])
    assert imps == {"pack1.mod1.smod2": "__py__@pack1.mod1.smod2"}

    imps = get_imports_py(["import pack1 as p1"])
    assert imps == {"p1": "__py__@pack1"}

    imps = get_imports_py(["import pack1,pack2"])
    assert imps == {"pack1": "__py__@pack1", "pack2": "__py__@pack2"}

    imps = get_imports_py(["import (pack1,pack2)"])
    assert imps == {"pack1": "__py__@pack1", "pack2": "__py__@pack2"}

    imps = get_imports_py(["from pack1 import def1"])
    assert imps == {"def1": "__py__@pack1.def1"}

    imps = get_imports_py(["from .pack1 import def1"])
    assert imps == {"def1": "__py__@pack1.def1"}


    imps = get_imports_py(["from pack1 import def1,def2"])
    assert imps == {"def1": "__py__@pack1.def1", "def2": "__py__@pack1.def2"}

    imps = get_imports_py(["from pack1 import def1,def2 as d1,d2"])
    assert imps == {"d1": "__py__@pack1.def1", "d2": "__py__@pack1.def2"}

    imps = get_imports_py(["from pack1 import (def1,def2) as (d1,d2)"])
    assert imps == {"d1": "__py__@pack1.def1", "d2": "__py__@pack1.def2"}

    imps = get_imports_py(["from pack1 import def1,def2 as (d1,d2)"])
    assert imps == {"d1": "__py__@pack1.def1", "d2": "__py__@pack1.def2"}

    imps = get_imports_py(["from pack1 import (def1,def2) as d1,d2"])
    assert imps == {"d1": "__py__@pack1.def1", "d2": "__py__@pack1.def2"}

    # multiple imports
    imps = get_imports_py(
        [
            "    import pack1",
            "    import pack2",
        ]
    )
    assert imps == {"pack1": "__py__@pack1", "pack2": "__py__@pack2"}

    # override import
    imps = get_imports_py(
        [
            "from pack1 import def2",
            "from pack2 import def2",
        ]
    )
    assert imps == {"def2": "__py__@pack2.def2"}

    # starry import
    imps = get_imports_py(["from pack1 import *"])
    assert imps == {"_pack1": "__py__@pack1.*"}

def test_find_class_inheritance():

    assert find_class_inheritance("") == []
    assert find_class_inheritance("class myObject") == []
    assert find_class_inheritance("class myObject()") == []
    assert find_class_inheritance("class myObject( abc )") == ["abc"]
    assert find_class_inheritance("class myObject( abc1, abc2 )") == ["abc1", "abc2"]



def test_build_py_ctrl_points():

     # void input
    ctrplt = build_py_ctrl_points([[]],[])
    assert ctrplt == []


    ref = """def check_conditions(x, y, a):
    if x > 0:
        if y > 0:
            y = 1.0
    if x > 0:
        if y > 0:
            y = 1.0
            if a == 'a':
                # Code for case a
                pass
            elif a == 'b':
                # Code for case b
                pass
            else:
                # Default case
                pass
    return y
"""

    tokencode = [tokenize(line) for line in ref.splitlines()]
    indents=  [ len(get_indent(line))/4 for line in ref.splitlines()]
    ctrplt = build_py_ctrl_points(indents,tokencode)
    #print(ctrplt)
    assert ctrplt ==['if' , ['if', []], 'if', ['if', ['if', [], 'elif', [], 'else', []]]]