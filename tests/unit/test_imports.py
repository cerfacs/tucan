"""
Unit test for tree.py file for python code
"""
from tucan.imports_main import stop_to_file, find_import_ref, remove_py_in_keys
from tucan.string_utils import generate_dict_paths


def test_stop_to_file():
    assert stop_to_file(["a", "b.f90", "c"]) == ["a", "b.f90"]
    assert stop_to_file(["a", "b", "c"]) == ["a", "b", "c"]


def test_imports_c():
    """
    The Classival C Cpp imports
    """
    tree_dict = {
        "src": {
            "includes": {"one.hpp": {"one": {}}, "two.hpp": {"one": {}}},
            "core": {"main.cpp": {}, "add.cpp": {}},
        }
    }
    paths = generate_dict_paths(tree_dict)
    ref = find_import_ref(paths, "one.hpp@*")
    assert ref == "src/includes/one.hpp"

    ref = find_import_ref(paths, "none.hpp@*")
    assert ref == "__external__/none.hpp"

    # capital letters shout work too
    tree_dict = {
        "Src": {
            "Includes": {"One.hpp": {"One": {}}, "Two.hpp": {"One": {}}},
            "Core": {"Main.cpp": {}, "Add.cpp": {}},
        }
    }
    paths = generate_dict_paths(tree_dict)
    ref = find_import_ref(paths, "One.hpp@*")
    assert ref == "Src/Includes/One.hpp"



def test_imports_ftn():
    """
    The Classical Fortran imports
    """
    tree_dict = {
        "src": {
            "pb_folder": {"block1": {}},
            "sources": {
                "main.h": {"block1": {}},
                "main.f90": {},
                "deprecated_main.h": {"block1": {}},
                "add.f90": {},
                "CamelCase.h": {"CamelBlock": {}},
                
            },
        }
    }
    paths = generate_dict_paths(tree_dict)
    ref = find_import_ref(paths, "main.h@*")
    assert ref == "src/sources/main.h"

    paths = generate_dict_paths(tree_dict)
    ref = find_import_ref(paths, "CamelCase.h@*")
    assert ref == "src/sources/CamelCase.h"


    ref = find_import_ref(paths, "__ftn__@block1", orig_file="src/sources/main.f90")
    assert ref == "src/sources/main.h"


def test_imports_py():
    """
    The Classical Python imports
    """
    tree_dict = {
        "src": {
            "template": {
                "dump": {},
                "compute": {},
            },
            "main": {
                "aaa.py": {
                    "a1": {},
                    "a2": {},
                },
                "bbb.py": {
                    "dump": {},
                    "compute": {},
                },
                "folder1": {"snoop.py": {"dump": {}, "compute": {}}},
            },
        }
    }
    paths = generate_dict_paths(tree_dict)
    # in python, you cannot tell if the first item is a file or a folder
    ref = find_import_ref(paths, "__py__@aaa")
    assert ref == "src/main/aaa.py"

    ref = find_import_ref(paths, "__py__@bbb.compute")
    assert ref == "src/main/bbb.py"

    ref = find_import_ref(paths, "__py__@bbb.dump.doit")
    assert ref == "src/main/bbb.py"

    ref = find_import_ref(paths, "__py__@folder1.snoop")
    assert ref == "src/main/folder1/snoop.py"

    ref = find_import_ref(paths, "__py__@folder1.snoop.compute")
    assert ref == "src/main/folder1/snoop.py"

    ref = find_import_ref(paths, "__py__@compute", orig_file="src/main/aaa.py")
    assert ref == "src/main/bbb.py"
