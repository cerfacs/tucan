from tucan.clean_ifdef import (
    interpret_ideflogic,
    replace_strings_by_proxies,
    interpret_ideflogic,
    replace_strings_by_proxies,
    align_continuation,
    remove_cpp_from_module,
)


def test_basic_ifdef():
    source = """
    lorem ipsum
#
    sic hamet
""".splitlines()
    target = """
    lorem ipsum

    sic hamet
""".splitlines()
    result = remove_cpp_from_module(source, [])
    for line, refline in zip(result, target):
        print(f"|{line}|<>|{refline}|")
        assert line == refline


SOURCE = """
#ifdef CWIPI
    ! Simple Ifdef
    WRITE(*,*) "CWIPI is enabled " ! Simple Ifdef
#elif CWIPI2
    WRITE(*,*) "CWIPI2 is enabled " ! Simple Ifdef
#else
    WRITE(*,*) "No CWIPI is enabled " ! Simple Ifdef
#endif
""".splitlines()


def test_ifdef_raw_else():
    result = remove_cpp_from_module(SOURCE, [])
    target = """






    WRITE(*,*) "No CWIPI is enabled " ! Simple Ifdef

""".splitlines()

    for line, refline in zip(result, target):
        print(f"|{line}|<>|{refline}|")
        assert line == refline


def test_ifdef_raw_ifdef():
    target = """

    ! Simple Ifdef
    WRITE(*,*) "CWIPI is enabled " ! Simple Ifdef





""".splitlines()
    result = remove_cpp_from_module(SOURCE, ["CWIPI"])

    for line, refline in zip(result, target):
        print(f"|{line}|<>|{refline}|")
        assert line == refline


def test_ifdef_raw_elif():
    target = """




    WRITE(*,*) "CWIPI2 is enabled " ! Simple Ifdef



""".splitlines()
    result = remove_cpp_from_module(SOURCE, ["CWIPI2"])

    for line, refline in zip(result, target):
        print(f"|{line}|<>|{refline}|")
        assert line == refline


def test_ifdef_align_continuation():
    source = """
dummY1
#define AAA \\
 BBB \\
 CCC
dummY2
""".splitlines()

    target = """
dummY1
#define AAA  BBB  CCC


dummY2
""".splitlines()
    result = align_continuation(source)

    for line, refline in zip(result, target):
        print(f"|{line}|<>|{refline}|")
        assert line == refline







def test_replace_strings_by_proxies():
    assert replace_strings_by_proxies("blue red") == ("blue red", {})

    assert replace_strings_by_proxies('blue "white black"') == (
        "blue #STR0#",
        {"#STR0#": '"white black"'},
    )

    assert replace_strings_by_proxies(
        'blue "white black" and last " '
    ) == (  # well this should never happen (3 "s) but you never know
        'blue #STR0# and last " ',
        {"#STR0#": '"white black"'},
    )

    assert replace_strings_by_proxies('blue "white black" and "red pink" end') == (
        "blue #STR0# and #STR2# end",
        {"#STR0#": '"white black"', "#STR2#": '"red pink"'},
    )


def test_ifdef_expression():
    # ifdef CWIPI
    assert interpret_ideflogic("#ifdef CWIPI", []) == False
    assert interpret_ideflogic("#ifdef CWIPI", ["CWIPI=True"]) == True

    assert interpret_ideflogic("#if defined(CWIPI)", []) == False
    assert interpret_ideflogic("#if defined(CWIPI)", ["CWIPI=True"]) == True

    assert (
        interpret_ideflogic(" #if defined(CWIPI)", []) == False
    )  # because some people like to add spaces where they should not

    assert interpret_ideflogic("#ifndef CWIPI", []) == True
    assert interpret_ideflogic("#ifndef CWIPI", ["CWIPI=True"]) == False

    assert interpret_ideflogic("#if !defined(CWIPI)", []) == True
    assert interpret_ideflogic("#if !defined(CWIPI)", ["CWIPI=True"]) == False

    assert interpret_ideflogic("# if !defined(CWIPI)", []) == True
    assert interpret_ideflogic("# elif !defined(CWIPI)", []) == True

    assert interpret_ideflogic("#if bad_name_and", []) == False
    assert interpret_ideflogic("#if bad_name_or", []) == False
    assert interpret_ideflogic("#if bad_name_not", []) == False

    assert interpret_ideflogic("#if CWIPI /* horrible comment */", []) == False

    assert (
        interpret_ideflogic(
            '#if CWIPI=="getting giggy with it"', ['CWIPI="getting giggy with it"']
        )
        == True
    )

    assert (
        interpret_ideflogic("#if EIGEN_GNUC_AT_LEAST(4,0)", []) == False
    )  # Horrible function-based expression. Triggering fallback

    assert (
        interpret_ideflogic("#if (FVMC_SIZEOF_DOUBLE != 8)", ["FVMC_SIZEOF_DOUBLE=8"])
        == False
    )

    assert interpret_ideflogic("#if CWIPI==40", ["CWIPI=40"]) == True
    assert interpret_ideflogic("#if CWIPI>40", ["CWIPI=30"]) == False
    assert interpret_ideflogic("#if CWIPI>40", ["CWIPI=50"]) == True

    assert interpret_ideflogic("#ifdef CWIPI || CWIPI2", []) == False
    assert interpret_ideflogic("#ifdef CWIPI || CWIPI2", ["CWIPI=True"]) == True
    assert interpret_ideflogic("#ifdef CWIPI || CWIPI2", ["CWIPI2=True"]) == True
    assert (
        interpret_ideflogic("#ifdef CWIPI || CWIPI2", ["CWIPI=True", "CWIPI2=True"])
        == True
    )

    assert interpret_ideflogic("#ifdef CWIPI && CWIPI2", []) == False
    assert interpret_ideflogic("#ifdef CWIPI && CWIPI2", ["CWIPI=True"]) == False
    assert interpret_ideflogic("#ifdef CWIPI && CWIPI2", ["CWIPI2=True"]) == False
    assert (
        interpret_ideflogic("#ifdef CWIPI && CWIPI2", ["CWIPI=True", "CWIPI2=True"])
        == True
    )
