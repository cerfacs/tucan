        PROGRAM FortranExample
#ifdef CWIPI
        ! Simple Ifdef
        WRITE(*,*) "CWIPI is enabled " ! Simple Ifdef
#elif CWIPI2
        WRITE(*,*) "CWIPI2 is enabled " ! Simple Ifdef
#else
        WRITE(*,*) "No CWIPI is enabled " ! Simple Ifdef
#endif
#if defined(FWIPI)
        WRITE(*,*) "FWIPI is enabled " ! Full version
#elif defined(FWIPI2)
        WRITE(*,*) "FWIPI2 is enabled " ! Full version
#else
        WRITE(*,*) "No FWIPI is enabled " ! Full version
#endif
#if defined(OUIPI1)||defined(OUIPI2)
        WRITE(*,*) "OUIPI is enabled "  ! Or
#endif

#ifdef FULL
       WRITE(*,*) " FULL is enabled " ! full subroutine
        SUBROUTINE dummy_full(a,b,c)
        END SUBROUTINE
#endif

#ifdef FRONT
        WRITE(*,*) " FRONT is enabled " ! partial front subroutine
        SUBROUTINE dummy_front(a,b,c)
        WRITE(*,*) " FRONT 1"     ! partial front subroutine
#else                
        SUBROUTINE dummy_front(a,d,e)
        WRITE(*,*) " FRONT 2"       ! partial front subroutine
#endif
        END SUBROUTINE

        SUBROUTINE dummy_back(a,b,c)
#ifdef BACK
        WRITE(*,*) " BACK is enabled " ! partial front subroutine
        WRITE(*,*) " BACK 1"    ! partial back subroutine
        END SUBROUTINE  
#else
        WRITE(*,*) " BACK 2"    ! partial back subroutine
        END SUBROUTINE  
#endif
        
        SUBROUTINE arg_extent(
     & a,b,c,  
#ifdef MOREARG
     & d,e,f, ! argument extension
#endif
     & x,y,z)
        END SUBROUTINE  

#ifdef SUB
        WRITE(*,*) "SUB is enabled, sub1 should come " ! sub ifdef
#define SUB1            
#else
            WRITE(*,*) "SUB is disabled, sub2 should come " ! sub ifdef
#define SUB2 100
#endif
#ifdef SUB1
        WRITE(*,*) "SUB 1 enabled " ! sub ifdef
#endif
#ifdef SUB2
        WRITE(*,*) "SUB 2 enabled "! sub ifdef
#endif
#ifdef LVL1
        WRITE(*,*) "LVL 1 enabled " ! nested ifdef
#ifdef LVL2
        WRITE(*,*) "LVL 1 &2 enabled " ! nested ifdef
#endif
#endif

#
        END PROGRAM FortranExample
