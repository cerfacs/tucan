from tucan.unformat_main import unformat_main

import json


def test_unformat_py(datadir):
    stmts = unformat_main(
        str(datadir) + "/to_unformat.py",
    )
    print(stmts)

    with open(str(datadir) + "/to_unformat.json", "r") as fin:
        ref_out = json.load(fin)

    # check length
    assert len(stmts.to_nob()) == len(ref_out)
    # check line by line

    for now, ref in zip(stmts.to_nob(), ref_out):
        assert now == ref
