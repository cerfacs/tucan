from tucan.clean_ifdef import scan_cpp_variables, remove_cpp_from_module

import json


def test_ifdef_scan(datadir):
    with open(str(datadir) + "/templates_ifdef.f", "r") as fin:
        lines = fin.read().split("\n")
    globals, locals = scan_cpp_variables(lines)

    with open(str(datadir) + "/templates_ifdef_ifdefs.json", "r") as fin:
        ref_out = json.load(fin)

    # check Global
    assert globals == ref_out["global"]
    # check loval
    assert locals == ref_out["local"]


def test_ifdef_clean(datadir):
    with open(str(datadir) + "/templates_ifdef.f", "r") as fin:
        lines = fin.read().split("\n")
    cleaned_lines = remove_cpp_from_module(lines, [])

    with open(str(datadir) + "/templates_ifdef.f_ifdef_resolved", "r") as fin:
        ref_lines = fin.read().split("\n")

    # check Global
    for i, line in enumerate(cleaned_lines):
        print(f"({i+1})|{line}|<>|{ref_lines[i]}|")
        assert line == ref_lines[i]
