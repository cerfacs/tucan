from __future__ import annotations


class myObject:
    def __init__(self):
        self.toto = 1.0
        pass

    def meth1(self, aaa):
        return external_fun(aaa)

    def meth2(self):
        pass

    def meth3(self):
        return self.meth2()


def external_fun(aaa):
    bbb = myObject()
    return 2 * aaa


def main(ttt: myObject):
    ttt.meth2()
    pass
