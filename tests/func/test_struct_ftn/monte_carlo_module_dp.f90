module monte_carlo_module_dp
  implicit none

contains

  subroutine generate_points_dp(n_points, n_hits)
    integer, intent(in) :: n_points
    integer, intent(out) :: n_hits
    double precision:: x, y
    integer  :: i

    ! Initialize random number generator
    call random_seed()

    ! Generate random points and count the number of hits
    n_hits = 0
    do i = 1, n_points
      call random_number(x)
      call random_number(y)
      if (is_inside_unit_circle_dp(x, y)) then
        n_hits = n_hits + 1
      end if
    end do

  end subroutine generate_points_dp

  function is_inside_unit_circle_dp(x, y) result(inside)
    double precision, intent(in) :: x, y
    logical :: inside

    ! Determine whether the point (x,y) is inside the unit circle
    inside = (x**2 + y**2 < 1.0)

  end function is_inside_unit_circle_dp

  print(*,*) "Toto"
  print(*,*) "Titi"
  print(*,*) "Titi"
  
end module monte_carlo_module_dp


pure function dummy1(x, y) result(inside)
  double precision, intent(in) :: x, y
  logical :: inside
  ! Determine whether the point (x,y) is inside the unit circle
  inside = (x**2 + y**2 < 1.0)

end function dummy1

elemental function dummy2(x, y) result(inside)
  double precision, intent(in) :: x, y
  logical :: inside
  ! Determine whether the point (x,y) is inside the unit circle
  inside = (x**2 + y**2 < 1.0)

end function dummy2

recursive function dummy3(x, y) result(inside)
  double precision, intent(in) :: x, y
  logical :: inside
  ! Determine whether the point (x,y) is inside the unit circle
  inside = (x**2 + y**2 < 1.0)

end function dummy3

TYPE   , PUBLIC:: DUAL_NUM
 	                              ! make this private will create difficulty to use the original write/read commands,
                                  ! hence x_ad_ and xp_ad_ are variables which can be accessed using D%x_ad_ and
    					          ! D%xp_ad_ in other units using this module in which D is defined as TYPE DUAL_NUM.
	REAL(DBL_AD)::x_ad_           ! functional value
	REAL(DBL_AD)::xp_ad_(NDV_AD)  ! derivative

END TYPE DUAL_NUM