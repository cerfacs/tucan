from tucan.unformat_main import unformat_main

import json


def test_unformat_ftn(datadir):
    stmts = unformat_main(str(datadir) + "/to_unformat.f")
    
    # with open(str(datadir) + "/to_unformat.json", "r") as fin:
    #     ref_out = json.load(fin)
    
    with open(str(datadir) + "/to_unformat._rfmt", "r") as fin:
        ref_out = fin.read().splitlines()
        ref_out.append("") #this reading miss the final line at the end

    # check line by line
    for now, ref in zip(stmts.to_code(), ref_out):
        #print("test:",now,"|")
        #print("ref :",ref,"|")
        assert now == ref

    # check length
    assert len(stmts.to_code()) == len(ref_out)
