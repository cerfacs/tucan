! This is a single-line comment using "!"
        PROGRAM FortranExample

        ! Declare variables
        INTEGER :: x, y
        REAL :: result
        INTEGER, DIMENSION(1:8), PARAMETER :: nledge = (/ 3,  4,  6,  8,  9,  12,  0, 0 /)

! ----------------------
! Multiple statements on the same line separated by semicolons
        INTEGER :: a, b, c
        a = 1; b = 2; c = 3
! ----------------------


! ----------------------
! continuations lines
        INTEGER, DIMENSION(1:4,1:2), PARAMETER :: next = RESHAPE ( (/  &
        ! tri:
            2,    3,    1,    0,   &
        ! quad:
            2,    3,    4,    1       /), (/4, 2/) )
        ! Initialize variables
        ! Same with an acceptable typo
        INTEGER, DIMENSION(1:4,1:2), PARAMETER :: next_two = RESHAPE ( (/  &
            21,    31,    11,    01,   &
            21,    31,    41,    11       ), (/41, 21/) )

        REAL, DIMENSION(5) :: my_array
        my_array = [ 1.0, 2.0, 3.0, &
            4.0, 5.0 ]

        x = 5
        y = 10
        ! Single-line comment using "!"
        result = 1.0 + 2.0 + 3.0 + &
        aaa ! bloult
        tmp =&
            & 4.0 + 5.0 ! This is an inline comment using "!"
        ! absolute asshole at play here
            a = 1; b = 2; c = 3; &
            d = 1; e = 2; f = 3 
            &; g = 1; h = 2; i = 3     ! Eat this you f******r
            j = 1; k = 2; l = 3; &
            & m = 1; n = 2; o = 3
            
        ! Using continuation lines
        CHARACTER(100) :: multiline_string
        multiline_string = 'This is a multiline string. ' // &
                           'You can break it into multiple lines.'
        
        ! apprently this on is illegal
        ! multiline_string2 = 'This is a multiline string.  &
        !                      You can break it into multiple lines.'

        ! Modern Fortran multiline string using "trim" and "adjustl"
        CHARACTER(1000) :: modern_multiline_string
        modern_multiline_string = TRIM('This is a multiline string with modern Fortran. ' // &
            'You can break it into multiple lines using TRIM and ADJUSTL.')

        DATA ((P(I,L),L=1,8),I=21,30) /
     1    .8962,  .8979,  .903 ,  .9114,  .923 ,  .9545,  .9955, 1.044 ,
     2    .8727,  .8741,  .878 ,  .8845,  .8935,  .918 ,  .9505,  .9893,
     3    .8538,  .8549,  .858 ,  .8632,  .8703,  .890 ,  .9164,  .9482,
     4    .8379,  .8388,  .8414,  .8456,  .8515,  .868 ,  .8895,  .916 ,
     5    .8243,  .8251,  .8273,  .8308,  .8356,  .8493,  .8676,  .89  ,
     6    .8018,  .8024,  .8039,  .8065,  .810 ,  .820 ,  .8337,  .8504,
     7    .7836,  .784 ,  .7852,  .7872,  .7899,  .7976,  .808 ,  .8212,
     8    .7683,  .7687,  .7696,  .771 ,  .7733,  .7794,  .788 ,  .7983,
     9    .7552,  .7554,  .7562,  .7575,  .7592,  .764 ,  .771 ,  .7797,
     *    .7436,  .7438,  .7445,  .7455,  .747 ,  .7512,  .757 ,  .7642/

        subroutine compute_ignit_source_terms( neq,ndim,nnode,dt,
     +                        dtsum,isptype,x,source,dw,voln,
     +                        t_width,r_width,t_center,x_center,
     +                        y_center,z_center,energy_cst,
     +                        ignit_vpower,isp1d )
        end subroutine compute_ignit_source_terms
! continuations lines
! ----------------------






        ! Multiline comment using "!" and "c"
        ! This is a multiline comment
        c This is another way to write a comment
        ! spanning multiple lines
        ! in old Fortran
        C This is another CAPITAL way to write a comment

!----------------------
! IF STATEMENTS
        ! Single IFTHENELSE   (I dont think we will need to split this...)
        result = IF(x == 5) THEN 1.0 ELSE 0.0

        IF (x > 0) 10, 20, 30

        IF(PRESENT(val4))THEN
            IF(res%x_ad_<val4%x_ad_) res=val4
        ENDIF

        ! Modern Fortran do loop
        DO i = 1, 10
            PRINT *, 'Iteration', i
        END DO

        IF (x == 5) THEN
        PRINT *, 'x is 5.'
        PRINT *, 'This is another statement in the same block.'
        END IF
    
! LABELLED AND BARE...

1       if (ir-l < M) then    ! strange first col label
        end if

        labeldo : DO i = 1, 10
            PRINT *, 'Iteration', i
        END DO

        node_loop01: &
        do inode = 1,pnode
          ipoin = lnods(inode, ielem) 
        enddo node_loop01
    
        labelif : if 1 > 2 then
            PRINT *, 'dummy'
        END IF

        ! Bare do
        DO 
            PRINT *, 'Iteration', i
        END DO

! strong ifs statements found in examples 
        if (el_grp%face2elem%val(2,ifa)==0) then
            if (el_grp%face2int_face_comm_index2%val( &
                  el_grp%face2elemface%val(2,ifa))==ifaint) then
               ielfa = el_grp%face2elemface%val(1,ifa)
            end if
        end if


        if (myworker == mymaster) then
            if (ndata > 0) then
               deallocate(data_ptrs)
            end if
        end if
        
        if (ifa1/=0) then
            if (i/=ifa1) then
            end if
        end if

! if with a ! inside : strings must be replaced BEFORE removing comments
        if(((markc=='!').or.(markc=='*')).and.(.not.found)) then  
            write(lisre,1) fword,texts(2:35)
            call runend('GETINT: COMPULSORY PARAM. NOT FOUND')
        end if

! more fun with & and ! intricated, what a laugh...
        if (decim<48.or.decim>57) then                 ! It is not a num.
            if (&
                 decim/= 43.and.decim/= 45.and. &       ! discard + -
                 decim/= 68.and.decim/= 69.and. &       ! discard D E
                 decim/=100.and.decim/=101.and. &       ! discard d e 
                 decim/= 46) then                       ! discard .
               lflag=1
               istri=nstri
            end if
        end if
         
! the legendary Chemkin DO shared loop
        DO 2000 K = 1, KK-1
            DO 2000 J = K+1, KK
                DO 2000 N = 1, NO
                    COFD(N,J,K) = COFD(N,K,J)
        2000 CONTINUE


! IF STATEMENTS
!----------------------

        STOP
        END PROGRAM FortranExample


!-----------------------
!types
        type bc_parameters_euler_t
            character(len=strl) :: bnd_name                             !< Boundary name
            character(len=strl) :: name                                 !< Name of the boundary type (periodic, neumann, dirichlet)
            integer             :: bc_type                              !< Boundary type: BCTYPE_OUTLET_SUPER, BCTYPE_SYMMETRY, etc.
        end type bc_parameters_euler_t

        type, extends(plasma_spec) :: plasma_euler_t
            type(bc_parameters_euler_t), dimension(:), allocatable :: patch_param_euler

            contains
            procedure :: init_plasma => init_plasma_euler
        end type

        select type(adv => this)
            type is(adv_no_dealias_t)
                if (allocated(adv%temp)) then
                    deallocate(adv%temp)
                end if
                if (c_associated(adv%temp_d)) then
                    call device_free(adv%temp_d)
                end if
        end select

        select case (pt_data_in)
            case (PT_DATATYPE_REAL_SCALAR)
                type_error = .false.
            case (DATATYPE_REAL_NODE_SCALAR)
                type_error = .false.
            case (DATATYPE_REAL_NODE_SCALAR2)
                type_error = .false.          
            case default
                type_error = .true.
        end select
!-----------------------
!less common elements
        interface
            ! over typed function
            real(c_double) function y2_physmem_total() bind(C,name="y2_physmem_total")
            use iso_c_binding
            end function
            ! over typed function
            integer(c_int) function y2_total_cores() bind(C,name="y2_total_cores")
            use iso_c_binding
            end function         
        end interface

        abstract interface
        end interface

        REAL( selected_real_kind(15,307) ) FUNCTION sgamma ( a )
        END FUNCTION
        
        eigenvalues: block
        real(WP) :: tr, discr_sq, discr           
        end block eigenvalues

            

     