from tucan.struct_main import struct_main
from tucan.cli_pprinter import struct_summary_file
import json


def test_struct_py(datadir):
    struct_ = struct_main(str(datadir) + "/objs.py")
    print(f"\nvvvvv found {len(struct_)} out of test vvvvvv")

    print(struct_summary_file(struct_))
    with open(str(datadir) + "/objs.json", "r") as fin:
        ref_out = json.load(fin)

    print(f"\nvvvvv found {len(ref_out)} out of JSON vvvvv")
    print(struct_summary_file(ref_out))

    # check keys
    print(sorted(struct_.keys()))
    print(sorted(ref_out.keys()))
    assert sorted(struct_.keys()) == sorted(ref_out.keys())
    # check line by line
    for key in struct_:
        assert struct_[key] == ref_out[key]
