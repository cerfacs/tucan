#include <stdio.h>
#include <iostream>
// Class declaration
class MyClass
{
public:
    // Member variables (data members)
    int myInt;
    double myDouble;

    // Member functions
    void displayValues()
    {
        std::cout << "Integer: " << myInt << "\n";
        std::cout << "Double: " << myDouble << "\n";
    }
};

int main()
{
    // Create an object of the class
    MyClass myObject;

    // Access and modify member variables
    myObject.myInt = 42;
    myObject.myDouble = 3.14;

    // Call a member function
    myObject.displayValues();

    return 0;
}

int main2()
{
    // Declare variables
    int x, y;
    float result;
    const int nledge[] = {3, 4, 6, 8, 9, 12, 0, 0};

    const int next[4][2] = {
        {2, 3},
        {1, 0},
        {2, 3},
        {4, 1}};

    x = 5;
    y = 10;

    // Single-line comment using "//"
    result = 1.0 + 2.0 + 3.0 +
             4.0 + 5.0; // This is an inline comment using "//"

    int a, b, c, d, e, f, g, h, i, j, k, l, m, n, o;

    a = 1;
    b = 2;
    c = 3;
    d = 1;
    e = 2;
    f = 3;
    g = 1;
    h = 2;
    i = 3; // Eat this you f******r
    j = 1;
    k = 2;
    l = 3;
    m = 1;
    n = 2;
    o = 3;

    // Single if-else statement
    if (x == 5)
        result = 1.0;
    else
        result = 0.0;

    // Using continuation lines for a multiline string
    char multiline_string[100];
    snprintf(multiline_string, sizeof(multiline_string), "This is a multiline string. "
                                                         "You can break it into multiple lines.");

    // Modern C multiline string
    char modern_multiline_string[1000];
    snprintf(modern_multiline_string, sizeof(modern_multiline_string), "This is a multiline string with modern C. "
                                                                       "You can break it into multiple lines using string concatenation.");

    // Multiline statements using braces for control flow
    if (x == 5)
    {
        printf("x is 5.\n");
        printf("This is another statement in the same block.\n");
    };

    // Multiple statements on the same line separated by semicolons
    int aa, bb, cc;
    aa = 1;
    bb = 2;
    cc = 3;

    // Continuing an array across multiple lines
    float my_array[5] = {
        1.0, 2.0, 3.0,
        4.0, 5.0};

    // Multiline comment using "/* ... */"
    /*
    This is a multiline comment
    This is another way to write a comment
    spanning multiple lines
    in C
    */

    // Modern C for loop
    for (int i = 1; i <= 10; i++)
    {
        printf("Iteration %d\n", i);
    };

    // End the program
    return 0;
};
