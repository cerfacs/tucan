from tucan.unformat_main import unformat_main

import json


def test_unformat_c(datadir):
    stmts = unformat_main(str(datadir) + "/to_unformat.cpp")
    print(stmts)

    with open(str(datadir) + "/to_unformat.json", "r") as fin:
        ref_out = json.load(fin)

    # check line by line
    for now, ref in zip(stmts.to_nob(), ref_out):
        print("now:",now)
        print("ref:",ref)
        assert now == ref

    # check length
    assert len(stmts.to_nob()) == len(ref_out)
