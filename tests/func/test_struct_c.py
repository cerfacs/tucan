from tucan.struct_main import struct_main
from tucan.cli_pprinter import struct_summary_file

import json


def test_struct_c(datadir):
    struct_ = struct_main(str(datadir) + "/montecarlo.c")

    print(f"found {len(struct_)} out of test")

    with open(str(datadir) + "/montecarlo.json", "r") as fin:
        ref_out = json.load(fin)
    print(f"found {len(ref_out)} out of JSON")

    print(struct_summary_file(ref_out))

    # check length
    assert len(struct_) == len(ref_out)
    # check line by line
    print(struct_)
    for now, ref in zip(sorted(struct_), ref_out):
        assert now == ref
