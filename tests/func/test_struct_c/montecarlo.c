#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int is_inside_unit_circle(double x, double y)
{
    // Determine whether the point (x, y) is inside the unit circle
    return (x * x + y * y < 1.0);
}

void generate_points(int n_points, int *n_hits)
{
    double x, y;
    *n_hits = 0;

    // Initialize random number generator
    srand(time(NULL));

    for (int i = 0; i < n_points; i++)
    {
        x = (double)rand() / RAND_MAX;
        y = (double)rand() / RAND_MAX;

        if (is_inside_unit_circle(x, y))
        {
            (*n_hits)++;
        }
    }
}

int main()
{
    int n_points = 1000000;
    int n_hits;

    generate_points(n_points, &n_hits);

    printf("Number of points inside the unit circle: %d\n", n_hits);

    double pi_estimate = 4.0 * n_hits / n_points;
    printf("Estimated value of pi: %lf\n", pi_estimate);

    return 0;
}
