# Multiple statements on the same line separated by semicolons
x = 5
y = 10

# Breaking a long line into multiple lines using a backslash (\)
result = 1 + 2 + 3 + 4 + 5

# Using parentheses to continue a statement across multiple lines
numbers = 1 + 2 + 3 + 4 + 5

# multiline blocks
a =  (
    a, 
    b, 
    c
)

a =  [
    a, 
    b, 
    c
]

prefix = "".join([f"{word.upper()} " for word in prefix.split("_")])
style_struct = build_style_struct(legend)
   
def blout():
    uglystring = " hello 'aaaa'bbb'cccc' "
    insanestring = f" hello 'aaaa{blit(arg1,arg2)}'bbb'cccc'  {blit(arg1,arg2)}"
    raise ValueError(
        "'fictive_tracer' treatment only accepted for 'inlet_pt_tt, 'inlet_relax_mass_flowrate', 'inlet_film_cooling' bc"
    )

def comment_in_line():
    line = "lorem ipsum"
    new_stmt = []
    if line.startswith("#"):  # add a hashtag in a string to make sure the comment cleaner does not harm the global clean
        new_stmt.append(line)
        pass

def blit(arg1, arg2):
    # Multiline strings using triple-quotes
    multiline_string = """This is a multiline string.
    You can break it into multiple lines.
    """

# Multiline strings using triple-quotes
multiline_string = """This is a multiline string.
You can break it into multiple lines.
"""

# Multiline strings with triple-quotes and escaping a newline
escaped_newline = "This is a string with a newline at the end.\
You can escape it to continue on the same line."


# Docstring for a function
def my_function(a=1, b=2):
    """
    This is a docstring.

    It provides information about the function.
    """
    a = 1
    b = 2
    c = 3
    return None


# Multiple statements in a block
if x == 5:
    print("x is 5.")
    print("This is another statement in the same block.")

# Multiple statements in a single line using semicolons
a = 1
b = 2
c = 3

# Continuing a list or dictionary across multiple lines
my_list = [1, 2, 3, 4, 5]  # inside comment

my_dict = {"key1": "value1", "key2": "value2", "key3": "value3"}
