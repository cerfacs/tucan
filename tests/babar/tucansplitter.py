"""
This module processes code files in a repository, analyzing their structure and splitting 
them into smaller chunks with optional overlaps. It utilizes the `tucan.package_analysis` 
library for structural analysis and facilitates chunk-based processing of code elements.
"""

from typing import List, Tuple
from tucan.package_analysis import run_struct
from tucan.travel_in_package import find_package_files_and_folders
import json

# Define the path to the repository
REPO_PATH = "./bsrepo"


def fetch_modules_in_repository(
    path: str,
    mandatory_patterns: List[str] = None,
    forbidden_patterns: List[str] = None,
) -> dict:
    """return the content of each module of a repository"""
    # Run structural analysis on the repository
    paths_dict = find_package_files_and_folders(
        path,
        mandatory_patterns=mandatory_patterns,
        forbidden_patterns=forbidden_patterns,
    )
    out = {}
    for file, fpath in paths_dict.items():
        with open(fpath, "r") as file:
            out["fpath"] = file.read().splitlines()
    return out


def split_code_into_chunks(
    code_lines: List[str],
    chunk_size: int,
    overlap_size: int,
) -> List[List[str]]:
    """
    Splits a list of code lines into smaller chunks with optional overlap.

    Args:
        code_lines (List[str]): The code lines to split.
        chunk_size (int): The number of lines in each chunk (excluding overlap).
        overlap_size (int): The number of overlapping lines between chunks.

    Returns:
        List[List[str]]: A list of code chunks, each prefixed with the header.
    """
    # Remove empty and trailing whitespace lines
    code_lines = [line.rstrip() for line in code_lines if line.strip()]
    total_lines = len(code_lines)

    # Calculate the number of chunks
    num_chunks = max(1, (total_lines + overlap_size) // (chunk_size + overlap_size))
    chunk_length = total_lines // num_chunks

    # Generate chunks
    chunks = []
    for i in range(num_chunks):
        start = max(0, i * chunk_length - overlap_size)
        end = min(total_lines, (i + 1) * chunk_length)
        chunks.append(code_lines[start:end])

    return chunks


def text_splitter_repository(
    folder: str,
    chunk_size: int,
    overlap_size: int,
    mandatory_patterns: List[str] = None,
    forbidden_patterns: List[str] = None,
    cpp_directives: list = None,
) -> Tuple[List[str], List[dict]]:
    """
    Processes a repository, splitting analyzed code files into chunks.

    Args:
        folder (str): The path to the repository folder.
        chunk_size (int): The number of lines in each chunk (excluding overlap).
        overlap_size (int): The number of overlapping lines between chunks.
        include_headers (bool): Whether to include a header in each chunk.

    Returns:
        List[str]: A list of code chunks as strings, ready for processing.
    """
    # Run structural analysis on the repository
    paths_dict = find_package_files_and_folders(
        folder,
        mandatory_patterns=mandatory_patterns,
        forbidden_patterns=forbidden_patterns,
    )

    full_analysis = run_struct(paths_dict, cpp_directives=cpp_directives)

    all_chunks = []
    all_metadata = []
    all_files = sorted(list(full_analysis.keys()))

    # Process each file and its elements
    for file_name in all_files:
        with open(f"{folder}/{file_name}", "r") as file:
            code_lines = file.readlines()

        for element_name, element_data in full_analysis[file_name].items():
            start, end = element_data["lines"]
            type = element_data["type"]
            element_body = code_lines[start:end]

            # Split the element body into chunks
            chunks = split_code_into_chunks(element_body, chunk_size, overlap_size)
            for i, chunk in enumerate(chunks):
                all_chunks.append(chunk)
                meta_data = {
                    "Element": element_name,
                    "file": file_name,
                    "start": start,
                    "type": type,
                    "end": end,
                    "part": f"{i+1}/{len(chunks)}",
                }
                all_metadata.append(meta_data)

    return all_chunks, all_metadata


# out = fetch_modules_in_repository(REPO_PATH)
# print(out)

# Process the repository and print the resulting chunks
chunks, meta_datas =  text_splitter_repository(REPO_PATH, chunk_size=10, overlap_size=2)

for idx, chunk in enumerate(chunks):
    print(f"Chunk {idx} ==============")
    print(json.dumps(meta_datas[idx]))
    print("\n".join(chunk))
