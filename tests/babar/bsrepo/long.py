def complex_function(input_list):
    """
    A function with over 40 unique statements for testing purposes.
    Args:
        input_list (list): A list of integers.
    Returns:
        dict: A dictionary with various computed results.
    """
    # Initialize variables
    result_dict = {}
    total = 0
    count = 0
    max_value = None
    min_value = None
    squared_values = []

    # Basic calculations
    for value in input_list:
        total += value
        count += 1
        squared_values.append(value ** 2)
        if max_value is None or value > max_value:
            max_value = value
        if min_value is None or value < min_value:
            min_value = value

    # Average calculation
    if count > 0:
        average = total / count
    else:
        average = 0

    # Conditional logic
    if count > 10:
        message = "Large list"
    elif count > 5:
        message = "Medium list"
    else:
        message = "Small list"

    # Additional calculations
    even_numbers = [x for x in input_list if x % 2 == 0]
    odd_numbers = [x for x in input_list if x % 2 != 0]
    sum_of_evens = sum(even_numbers)
    sum_of_odds = sum(odd_numbers)

    # Nested loops and conditionals
    unique_pairs = []
    for i in range(len(input_list)):
        for j in range(i + 1, len(input_list)):
            pair_sum = input_list[i] + input_list[j]
            if pair_sum % 2 == 0:
                unique_pairs.append((input_list[i], input_list[j]))

    # String manipulations
    concatenated = "".join([str(x) for x in input_list])
    reversed_string = concatenated[::-1]
    contains_zero = "0" in concatenated

    # Set operations
    unique_values = set(input_list)
    duplicates = len(input_list) - len(unique_values)

    # Mathematical operations
    product = 1
    for num in input_list:
        product *= num

    # Use of external functions
    def factorial(n):
        if n == 0 or n == 1:
            return 1
        else:
            return n * factorial(n - 1)

    factorial_of_first = factorial(input_list[0]) if input_list else None

    # Final assignment
    result_dict["total"] = total
    result_dict["count"] = count
    result_dict["average"] = average
    result_dict["max"] = max_value
    result_dict["min"] = min_value
    result_dict["message"] = message
    result_dict["squared_values"] = squared_values
    result_dict["even_numbers"] = even_numbers
    result_dict["odd_numbers"] = odd_numbers
    result_dict["sum_of_evens"] = sum_of_evens
    result_dict["sum_of_odds"] = sum_of_odds
    result_dict["unique_pairs"] = unique_pairs
    result_dict["concatenated"] = concatenated
    result_dict["reversed_string"] = reversed_string
    result_dict["contains_zero"] = contains_zero
    result_dict["unique_values"] = list(unique_values)
    result_dict["duplicates"] = duplicates
    result_dict["product"] = product
    result_dict["factorial_of_first"] = factorial_of_first

    # Print statements for debug (optional)
    print("Debugging information:")
    for key, value in result_dict.items():
        print(f"{key}: {value}")

    # Return the dictionary
    return result_dict
