class Point:
    """
    Represents a point in 2D space.
    """

    def __init__(self, x=0, y=0):
        """
        Initializes a Point instance.
        Args:
            x (float): The x-coordinate of the point.
            y (float): The y-coordinate of the point.
        """
        self.x = x
        self.y = y

    def distance_from_origin(self):
        """
        Calculates the distance of the point from the origin (0, 0).
        Returns:
            float: The distance from the origin.
        """
        return (self.x ** 2 + self.y ** 2) ** 0.5

    def translate(self, dx, dy):
        """
        Translates the point by a given delta.
        Args:
            dx (float): The change in the x-coordinate.
            dy (float): The change in the y-coordinate.
        """
        self.x += dx
        self.y += dy

    def __str__(self):
        """
        Returns a string representation of the point.
        Returns:
            str: The string representation in the format "(x, y)".
        """
        return f"({self.x}, {self.y})"


def add_vectors(v1, v2):
    """
    Adds two 2D vectors.
    Args:
        v1 (tuple): The first vector as (x1, y1).
        v2 (tuple): The second vector as (x2, y2).
    Returns:
        tuple: The resulting vector as (x, y).
    """
    return v1[0] + v2[0], v1[1] + v2[1]


def dot_product(v1, v2):
    """
    Calculates the dot product of two 2D vectors.
    Args:
        v1 (tuple): The first vector as (x1, y1).
        v2 (tuple): The second vector as (x2, y2).
    Returns:
        float: The dot product of the two vectors.
    """
    return v1[0] * v2[0] + v1[1] * v2[1]


def scale_vector(v, scalar):
    """
    Scales a 2D vector by a scalar.
    Args:
        v (tuple): The vector as (x, y).
        scalar (float): The scaling factor.
    Returns:
        tuple: The scaled vector as (x, y).
    """
    return v[0] * scalar, v[1] * scalar
