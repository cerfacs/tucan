def add_numbers(a, b):
    """
    Adds two numbers together.
    Args:
        a (int or float): The first number.
        b (int or float): The second number.
    Returns:
        int or float: The sum of a and b.
    """
    return a + b


def is_even(number):
    """
    Checks if a number is even.
    Args:
        number (int): The number to check.
    Returns:
        bool: True if the number is even, False otherwise.
    """
    return number % 2 == 0


def reverse_string(s):
    """
    Reverses a given string.
    Args:
        s (str): The string to reverse.
    Returns:
        str: The reversed string.
    """
    return s[::-1]
