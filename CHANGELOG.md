# Changelog

All notable changes to this project will be documented in this file.
The format is based on Keep a Changelog,
and this project adheres to Semantic Versioning.



## [0.4.5] 2024 / 12 / 19


### Fixed

- Fixing bug on mandatory patterns and excluded patterns.


## [0.4.4] 2024 / 11 / 29


### Fixed

- cleaning python dicstring again , besause some ASCII art docstrings were blocking the parenthesis parser.
- error on file aggregation nor fixed. Bug was introduced by "no more `file` node" deprecation of 0.4.3

## [0.4.3] 2024 / 11 / 27

### Changed

- most `ifdef` functions are now refered as `cpp` functions

### Fixed

- Most CLIs commands include the `-cpp` option to provide CPP directives for interpretation
- Codesbases featuring (accidentally probably) character `Ì` are now working correctly
- C control loop syntax detection is fixed

### Deprecated

- no more `file` node at the root of each "structure" datasets.




## [0.4.2] 2024 / 10 / 10

### Added

- Add "time to code 50 lines " in struct repo log
- Add tracking of explicit loops (LPS)
- `find_package_files_and_folders`addition of forbidden_patterns, mandatory_patterns
- Add fortran Keywords ANY and WHERE to control points

### Changed

- `tucan struct-repo` dumps struct-repo.json and struct-files.json now

### Fixed

- Fix bug in data aggregation
- Security for Fortran use of reserved keeywords such as "integer :: CALL" or subroutine "INTERFACES"
- Fix bug in removing new fortran labels

### Deprecated
- `find_package_files_and_folders` suppression of optional_paths, too complex to handle


## [0.4.1] 2024 / 10 / 4

### Added

- Add function of struct_repo with proper aggregation of data 
- Nb of loops  counts
- a root object of type `file` is now added to file analysis

### Changed

- McCabe complexity limited to nb of ctrol points for the moment (un satisfatory)
- Npath complexity is removed from datas (not statisfactory)
- Struct agregation and Repo aggregation are handled separately

### Fixed

- Active and passive control points are now correctly sent to cplxity estimators
- Fortran handling of "labelled labelling do's" (110 do 120 i = 1,l)
- Fortran handling of strings with multiline in the middle
- Fortran handling of `subroutine` called "interface" (haha)
- Fortran support of `forall` one-liners

### Deprecated

- utilities for file exploration `rec_travel_through_package`and `get_package_files` are replaced by `find_package_files_and_folders`. Former was too fragile.

## [0.4.0] 2024 / 09 / 12

### Added

- Halstead complexity
- Maintainability index
- Npath complexity
- Add custom error TucanError for better re-use

### Fixed

- Improve speed of parsing 
- Handle  nested IF and WHERE in fortran on-liners
- Handle the . continuation pattern in fortran
- Fix nested labelled IF and labelled DO in fortran
- Remove tabs in code lines

## [0.3.2] 2024 / 07 / 26

### Changed

- new support for generic import detection
- more unitary tests for atomic string functions

### Fixed

- handle single # in line for ifdefs...

## [0.3.1] 2024 / 07 / 02

### Changed

- improved handling of template detection 
- many supplementary unit tests for C/C++
- CPP handling now done using tokenization

### Fixed

- add python support of indentations that are not 4 spaces


## [0.3.0] 2024 / 06 / 10

### Added

- support for c++ templates naming
- using a tokenizer to parse in most of the cases now

### Changed

- link computation made differently and tested (still unperfect tho)

### Fixed

- self references in Fortran and C code
- guess language more robust
- fix latin1 encoding bug
- capacity to split procedure declarations in Fortran
- handling of fortran Type keyword
- cpp surrogate can handle multiple spaces now in declarations

## [0.2.0] 2024 / 05 / 14

### Added

- initial release for external use

### Changed

[ - ]

### Fixed

[ - ]

### Deprecated

[ - ]
