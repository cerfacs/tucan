
# Backlog

- [STRUCT] Ajouter HVL Halstead volume aux données traitées
- [STRUCT] Ajouter lines a sauter aux données traitées
- [STRUCT] ajouter le langage aux données traitées 

---
*next version*
--- 

- ajouter le support JSON/YAML
- [STRING_UTILS] Proposer solution unique pour renvoyer le contenu purgé d'une procedure
- [CODESPLITTER] ajouter l'aide comme metadonnée 
- [CODESPLITTER] basculer en tokens
- Ajouter le grep par procedure?
- inclure l'evaluation du score grammaire dans Tucan
- [STRUCT] ajouter le resultat du score si demandé
