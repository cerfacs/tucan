test:
	PYTHONDONTWRITEBYTECODE=1 pytest tests --cov=tucan -v -x 

test_unit:
	PYTHONDONTWRITEBYTECODE=1 pytest tests -k unit --cov=tucan -v -x 

test_html:
	PYTHONDONTWRITEBYTECODE=1 pytest tests --cov=tucan --cov-report html


init:
	pip install -r requirements.txt

lint:
	pylint tucan

wheel:
	rm -rf build
	rm -rf dist
	python3 setup.py sdist bdist_wheel

upload_test:
	twine upload --repository-url https://test.pypi.org/legacy/ dist/*

upload:
	twine upload dist/* -u __token__

.PHONY: init
