# Complexities


The "struct" data from `tucan is shown as :

```
Ctls. Pts. (McCabe)    1          | 1          Int. avg.
Halstead Difficulty    11.05      | 11.05      Int. avg.
Maintainability Index  69.77      | 69.77      Int. avg.
Average indents        2.18       | 2.18       Int. avg.
Halstead time          3.12 min   | 3.12 min   Ext. avg.
Structural complexity  1          | 1          Ext. avg.
Nb. of Loops           0          | 0          Ext. avg.
```

## Cyclomatic complexity

In `tucan`, the Cyclomatic complexity is called CCN, or Cyclomatic Complexity Number (like in the package `lizard`). This comes from [the work of Mc Cabe](https://en.wikipedia.org/wiki/Cyclomatic_complexity).
In `tucan`, the CCN presently included *is limited to the  number of control points* inside a procedure (if, else). Control points are defined for each language in the file `kw_lang.py`.

There is also the capacity of computing the "Npath" complexity, closer to the original definition of McCabe.
However, as this is more fragile and computationaly expensive, the *"Npath" definition is currently not active*


The Cyclomatic complexity denotes the potential combinations possible for a single procedure. 
A high CCN denote a piece of code harder to test. Moreover, high CCN comes with a large mental load for the reader, making the code difficult to understand. CCN is aggregated intensively as CCN_int (each procedure averages its metric and the inner procedures, using NLOC ponderations).

## Average indents

The indentation level (IDT) as a complexity indication was introduced by [Adam Tornhill](https://en.wikipedia.org/wiki/CodeScene). Each control point and loop is usually involving a new indentation level. As a consequence, high indentations mean a large number of nested contexts. 

As for CCN, high IDT comes with a large mental load for the reader, making the code difficult to understand.  IDT is aggregated intensively as IDT_int (each procedure averages its metric and the inner procedures, using NLOC ponderations).

## Halstead Difficulty,  Halstead Time

The [Halstead metrics](https://en.wikipedia.org/wiki/Halstead_complexity_measures) are computed from :

- The number of operators i.e. punctuations (`(],;+`), intrinsics keywords ( `if,then,for`)
- The number of operands i.e. values (`3.14, "dummy"`), variables ( `aaa, bbb.ccc`)
- The number of unique operators
- The number of unique operands

A complex code base, in the sense of Halstead, requires a large variety of operands/operators will to express its functionality. Note that copy/pasted code does not increase a lot the Halstead complexity, contrarily to CCN.

From these informations, Hasltead proposed several metrics in particular : 
- the Halstead Difficulty (HDF) which denotes the variety of operators/operands with respect to the code size. HDF is aggregated intensively as HDF_int (each procedure averages its metric and the inner procedures, using NLOC ponderations).
- the Halstead Time (HTM) which could be *the time to rewrite the codebase, knowing the algorithm*. HTM is aggregated extensively as HTM_ext (each procedure cumulates the inner procedures)



## Structural Complexity

The Structural complexity (CST) is a `tucan` experimental metric we work on to denote the mental load of advanced objects.

For example in fortran, the current costs used are:

| object | cost |
|-|-|
|        "module"| 1 |
|        "function"| 1|
|        "subroutine"| 2|
|        "interface"| 4|
|        "procedure"| 4|
|        "type"| 6|
  
These arbitrary costs  are (for the moment) the average number of reads necessary to get at ease with one structure.


CST is aggregated extensively as CST_ext (each procedure cumulates the inner procedures)

## Maintainability index

The [maintainability index](https://fr.wikipedia.org/wiki/Indice_de_maintenabilit%C3%A9) (MI) was was proposed by Paul Oman and Jack Hagemeister at the International Conference on Software Maintenance with the goal of establishing automated software development metrics to guide “software related decision making”. 

In `tucan` is computed from the Cyclomatic Complexity and the Halstead volume only.

The effect of comments is neglected, since comment practices varies too much in the HPC software engineering. 

Note that using this definition, the best MI is achieved with a void file and is 378,062!.

MI is aggregated intensively as MI_int (each procedure averages its metric and the inner procedures, using NLOC)