


Tutorials
=========

These are the fastest guides to learn about this package.
Go there if you are in a hurry.

.. toctree::
    :maxdepth: 2

    

How-to Guides
=============

These are the step-by-step guides to see a practical usage of this package.
Read this to learn by examples.

.. toctree::
    :maxdepth: 2

    
References
==========

This the exhaustive api description ressource.

.. toctree::
    :maxdepth: 2


Explanations
============

This is where you will find some in-depth explanation.
If your question starts with "why ?", the answer is probaly here.

.. toctree::
    :maxdepth: 2
    complexities.md

    
    
